# Homogeneity Enabled Incremental Dipole Inversion (HEIDI) algorithm

## Authors

### Ferdinand Schweser - University at Buffalo
**Role:** Creator, Algorithm Design, Implementation, Testing, and Optimization

### Karsten Sommer - Friedrich Schiller University Jena
**Role:** Algorithm Design, Testing, and Optimization

### Fahad Salman - University at Buffalo
**Role:** Implementation, Testing, and Optimization

## Contributors

### Andreas Deistung - Friedrich Schiller University Jena
**Role:** Bug fix

## Important notes

### Computational Environment
These Matlab functions cannot be run directly without major modifications. These functions were designed for containerized processing with Singularity at the University at Buffalo Center for Computational Research high performance cluster (HPC; NIH R01NS114227) using pi4s (https://gitlab.com/R01NS114227/pi4s). User who would like to run the functions in their own computational environment must adapt the code appropriately. Included scripts and configuration files provide all necessary information to understand how we ran the functions at out HPC. Users may want to review the `makefile` and `def.sh` (bash script to call LSQR and HEIDI slurm scripts), as well as the Slurm files (`inversion_lsqr.slurm` and `inversion_heidi.slurm`) to understand how the code was embedded in our greater HPC processing pipeline. 

The code has the following dependencies: 
- MATLAB R2018b
- Freesurfer 6.0.0
- FSL 5.0.8
- ANTs 2.0.0

### MRI Image Data Set (MIDS) Class
Our code relies on a custom Matlab class called MIDS. The class definition is provided in the repository: `mids.m`


## Main functions [LSQR and HEIDI]
There are two main processing functions for HEIDI:
`inversion_lsqr.m` – input to HEIDI computed with minimally regularized LSQR
`inversion_heidi.m` – HEIDI processing

### How to call the functions

```
inversion_lsqr(strAbsFile,strPhsFile,strMskFile,strOutFile,strOutFile_nii)
inversion_heidi(strAbsFile,strPhsFile,strMskFile,strSusFile,strOutFile,strOutFile_nii)
```

All function arguments are strings:

`strAbsFile` = path to a mat file containing a mids-object of the magnitude images  
`strPhsFile` = path to a mat file containing a mids-object of the background corrected field (phase) map (in radians)  
`strMskFile` = path to a mat file containing a mids-object of the mask  
`strSusFile` = path to a mat file containing the result of `inversion_lsqr.m`  
`strOutFile` = path to the mat file containing output in the form of mids-object  
`strOutFile_nii` = path to the NIfTI output of the inversion (.nii.gz)  

We recommend reviewing the corresponding Slurm files for a deeper understanding of how these functions are called.

### Pulse sequence configuration
Pulse sequence information may have to be adjusted manually in `inversion_lsqr.m` and `inversion_heidi.m`:

`Options.echoTime` = echo times in ms  
`Options.Rotation` = B0 direction  
`Options.magneticFieldStrength` = field strength in tesla (T)  
`Options.voxelAspectRatio` = voxel size  

## Citation
Please acknowledge any use these functions by cite the following publication:

Schweser F, Sommer K, Deistung A, Reichenbach JR. Quantitative susceptibility mapping for investigating subtle susceptibility variations in the human brain. Neuroimage. 2012 Sep;62(3):2083-100. doi: 10.1016/j.neuroimage.2012.05.067. Epub 2012 Jun 1. PMID: 22659482.