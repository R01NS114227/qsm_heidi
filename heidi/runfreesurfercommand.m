function shellStatusCode = runfreesurfercommand(commandString, toolbox, Options)
%RUNFREESURFERCOMMAND runs freesurfer commands on shel
%  
% shellStatusCode = runfreesurfercommand(commandString, toolbox)
%
%
% This function executes unix commands using the matlab sytem command on a 
% unix-shell. Since several functions commands require adacquate setting of 
% environment parameters, both the freesurfer and fsl environment are
% initialized at first, before excuting the commandString. 
%
%   toolbox - string that denotes the toolbox where the command belongs to.
%
%   Options - structure that is piped to exec_system_command and may
%             contain the following fields: 
%        *  noErrorOnShellErrorCode   - boolean. Tolerates shell error status codes.
%                                       This may be used if the shell status code
%                                       is erroneously set to error although no
%                                       error occured. (default: false)
%        * noShellCommandPrint       -  boolean. Prevents output of the shell
%                                     command. (default: false)

%              
%
%   shellStatusCode is the shell code (0=success).
%
% ChangeLog
% v1.0   - 2011/06/07 - A Deistung    - initial version
% v1.01  - 2011/06/21 - A Deistung    - commandString is now printed out
% v1.02  - 2011/06/21 - A Deistung    - export is used instead of set in
%                                       setting variables to the current bash
% v1.1   - 2011/06/30 - A Deistung    - bash is explicitely started before
%                                       running shell commands
% v1.2   - 2011/07/08 - A Deistung    - the ANTs environment is now also
%                                       initialized in the shell
% v1.3   - 2011/07/14 - A Deistung    - the DEFAULTTMP is initialized as
%                                       subject_dir, because some functions
%                                       require the subjects_dir (e.g., tkregister2)
% v1.3.1 - 2011/09/12 - A Deistung    - hostname removed from system call
% v1.3.2 - 2012/08/13 - F Schweser    - now uses dispstatus
% v1.3.3 - 2012/09/04 - F Schweser    - ANTS path was updated
% v1.4   - 2013/10/02 - F Schweser    - Now returns shell code
% v1.5   - 2013/10/10 - A Deistung    - FSL-bin is now added to the path
%                                       variable
% v1.6   - 2013/30/10 - A Deistung    - /opt/medphys/bin is now added to
%                                       the path variable
%
% v1.7   - 2013/11/04 - F Schweser    - LD_LIBRARY_PATH added
% v1.8   - 2013/12/03 - F Schweser    - now uses exec_system_command
% v1.9   - 2013/12/05 - A Deistung    - Options structure is now piped to
%                                       exec_system_command
% v2.0   - 2014/03/14 - F Schwese     - Added global variables for environment
%                                       and moved CPU architecture
%                                       detection to initializeswitoolbox
% v2.1   - 2015/01/12 - F Schweser    - changed fsl path for FSL

%%                                       

global GLOBAL_TEMPDIR
global GLOBAL_PATH_FREESURFER
global GLOBAL_PATH_ANTS
global GLOBAL_PATH_FSL
%global GLOBAL_FILE_FREESURFER_SETENV
global GLOBAL_FILE_ANTS_SETENV
if nargin < 3
    Options.dummy = [];
    if nargin < 2
        toolbox = [];
    end
end

DEFAULT_TMPDIR = GLOBAL_TEMPDIR;


if ~isempty(toolbox)
    switch lower(toolbox)
        case 'fsl'
            pathstring = fullfile(GLOBAL_PATH_FSL,'/bin/');
        case 'ants'
            pathstring = fullfile(GLOBAL_PATH_ANTS,'/bin/');
        otherwise
            warning('MATLAB:runfreesurfercommand', 'Only the following parameters are supported: FSL and ANTS.')
    end
else
    pathstring = '';
end

setenv('SHELL', '/bin/bash');
shellcommand = ['SUBJECTS_DIR=' DEFAULT_TMPDIR '; ', ...
                'export FREESURFER_HOME=' GLOBAL_PATH_FREESURFER ,'; ', ...
                'source $FREESURFER_HOME/SetUpFreeSurfer.sh; ', ...
                'source ' GLOBAL_FILE_ANTS_SETENV '; ', ...
                'export FSLOUTPUTTYPE=NIFTI_GZ; ', ...  % changed from NIFTI (un-gzipped)
                'export FSLDIR=', GLOBAL_PATH_FSL '; ', ... %'export FSLDIR=', GLOBAL_PATH_FREESURFER, 'fsl; ', ... Changed to actually use GLOBAL_PATH_FSL
                'export PATH="' fullfile(GLOBAL_PATH_FSL, 'bin/') ':$PATH"; ', ... %'export PATH=/opt/medphys/bin:$PATH; ', ...   % FS March 2014: I removed this line because I did not understand what it is good for. If you find out, please let me know!
                pathstring, commandString];
            % 'LD_LIBRARY_PATH= ', ... % FS March 2014: I removed this line
            % because it prevented execution on my system. Looks to me as
            % if this is required in Jena, something is wrong in the
            % configuration there (have a clean install)
            
%'unset SUBJECTS_DIR; ', ... 
            
dispstatus(shellcommand,[],'unix');
shellStatusCode = exec_system_command(shellcommand, Options);
end

