function [OutputArrayObj] = diffusionimagefilter(InputArrayObj,Options)


%DIFFUSIONIMAGEFILTER Applies an anisotropic diffusion filter to InputArray
%
%
%   Syntax
%
%   B = DIFFUSIONIMAGEFILTER(A)
%   B = DIFFUSIONIMAGEFILTER(A,Options)
%
%
%   Description
%
%   COMPUTESUSCEPTIBILITY(A) applies an anisotropic diffusion filter to input array A 
%   or mids A (2D/3D/4D).
%   see https://mri.radiology.uiowa.edu/mediawiki/index.php/Anisotropic_Diffusion_Example for details
%
%   COMPUTESUSCEPTIBILITY(A,Options) uses the options that are defined in
%   the struct Options for the algorithm.
%
%
%   The following Option-fields of are supported:
%       numberOfIterations      - Number of iterations that the filter will run 
%       timeStep                - time step for each iteration
%       conductance             - Parameter governing sensitivity of the
%                                 conductance equation
%       voxelsize               - defines the voxelsize (optional). If this
%                                 option is not set, the voxel size is
%                                 either extracted from mids object or (if
%                                 not mids object) set to isotropic
%
%
%   K Sommer, kar_sommer@gmx.de, 2010/12/03
%
%   Changelog:
%   v1.1 - 2010/12/06 - K Sommer -  now uses temporary file names
%   v1.2 - 2010/12/06 - F Schweser - Bugfix: default parameters + now supports mids
%   v1.3 - 2010/12/06 - F Schweser - Bugfix: temporary filename failed
%   v1.4 - 2011/03/17 - F Schweser - Now works with mids property voxel
%                                    size + now works with 2D and 4D data.
%   v1.5 - 2011/04/12 - F Schweser - Bugfix: Did not work without Options +
%                                    Default number of iterations set to 2
%   v1.51 - 2011/12/08 - F Schweser - dispstatus reset removed
%   v1.52 - 2011/12/13 - F Schweser - Minor bugfix regarding write_nii
%   v1.53 - 2012/06/05 - A Deistung - minor bugfix if inputArray is no object
%   v1.54 - 2012/03/04 - A Deistung - tmp-folder was modified
%   v1.6  - 2013/11/25 - F Schweser: Now uses exec_system_command instead
%                                    of system()
%   v1.7  - 2014/03/15 - F Schweser: GLOBAL_TEMPDIR
%   v1.8  - 2014/03/28 - F Schweser: Modification to new repos structure
%
%
%% constants

DEFAULT_numberOfIterations = 2;  
DEFAULT_timeStep = 0.125;
DEFAULT_conductance = 1;
global GLOBAL_TEMPDIR
DEFAULTTMPDIRECTORY             = GLOBAL_TEMPDIR;
%% Check input arguments

if nargin < 1 || isempty(InputArrayObj)
    error('Function requires at least one input argument.')
end

if isobject(InputArrayObj)
    InputArray = InputArrayObj.img;
else
    InputArray = InputArrayObj;
end
   
if nargin < 2 || isempty(Options)
    Options.dummy = [];
    dispstatus(mfilename(),Options,'functioncall')
    dispstatus('Default parameters will be used.',Options)
    Options.numberOfIterations = DEFAULT_numberOfIterations;
    Options.timeStep = DEFAULT_timeStep; 
    Options.conductance = DEFAULT_conductance;
else
    dispstatus(mfilename(),Options,'functioncall')
    if ~isfield(Options,'numberOfIterations')
        Options.numberOfIterations = DEFAULT_numberOfIterations;
    end
    if ~isfield(Options,'timeStep')
        Options.timeStep = DEFAULT_timeStep;
    end
    if ~isfield(Options,'conductance')
        Options.conductance = DEFAULT_conductance;
    end
end

nDimInput = ndims(InputArray);
if nDimInput < 3
    InputArray = repmat(InputArray,[1 1 3]);
    nVolume = 1;
elseif nDimInput == 3
    nVolume = 1;
elseif nDimInput == 4 %multi echo
    nVolume = size(InputArray,4);
else
    error('This function currently supports only 2D/3D/4D data.')
end

if ~isfield(Options, 'voxelsize')
    if isobject(InputArrayObj)
        Options.voxelsize = InputArrayObj.getvoxelsize;
        dispstatus(['Voxel size determined automatically from mids object: ',num2str(Options.voxelsize)],Options);
    else
        Options.voxelsize = [1, 1, 1];
        dispstatus('Voxel size set to isotropic (DEFAULT)',Options);
    end
else
    dispstatus(['Voxel size set manually: ',num2str(Options.voxelsize)],Options);
end


%% Apply Filter

tmpname = tempname();
name_in = fullfile([tmpname,'_in.nii']);
name_out = fullfile([tmpname,'_out.nii']);

OutputArray = zeros(size(InputArray,1),size(InputArray,2),size(InputArray,3),nVolume);
for jVolume = 1:nVolume
    write_nii(InputArray(:,:,:,jVolume), name_in, Options.voxelsize,[],[],[],true);

    repositoryPath = reppath;
    %exec_system_command(['chmod a+x ' fullfile(repositoryPath,'common_binary/ITK/VesselSegmentationProject/GradientAnisotropicDiffusionImageFilter')]);
    shellcommand = [ 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:' fullfile(repositoryPath) ';' ...
    fullfile(repositoryPath,'GradientAnisotropicDiffusionImageFilter') ' '...
    name_in ' ' name_out ' ' num2str(Options.numberOfIterations) ' ' num2str(Options.timeStep) ' ' num2str(Options.conductance)];

    exec_system_command(shellcommand);


    tmpArray = load_nii_raw(name_out);
    OutputArray(:,:,:,jVolume) = tmpArray.img;
end


exec_system_command(['chmod 777 ', name_in]);
exec_system_command(['chmod 777 ', name_out]);
exec_system_command(['rm ', name_in]);
exec_system_command(['rm ', name_out]);



if nDimInput < 3
    OutputArray = OutputArray(:,:,1);
end

if isobject(InputArrayObj)
    OutputArrayObj = InputArrayObj;
    OutputArrayObj.img = OutputArray;
else
    OutputArrayObj = OutputArray;
end

dispstatus(mfilename(),Options,'functionexit',true)

end

