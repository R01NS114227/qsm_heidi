function [ElementExp CropArray] = zealouscrop(ElementObj, cropArray, frameValue, bUndo,makeOdd)

%ZEALOUSCROP
%
%   Syntax
%
%   X = ZEALOUSCROP(A)
%   X = ZEALOUSCROP(A,c)
%   X = ZEALOUSCROP(A,[],v)
%   X = ZEALOUSCROP(A,[],v,true)
%   [X c] = ZEALOUSCROP(...)
%
%
%   Description
%
%   X = ZEALOUSCROP(A) removes all homogeneous slices, i.e. slices where all
%   elements are equal, from array A.
%
%   X = ZEALOUSCROP(A,c) crops A with information supplied by vector c. The
%   vector may be obtained from the function itself by using two output
%   variables.
%
%   X = ZEALOUSCROP(A,[],v) only slices where all values are equal to v
%   (real scalar) are removed. This makes the output of the function more
%   reliable in some extreme cases.
%
%   X = ZEALOUSCROP(A,c, [], bUndo) the cropping of the cropped matrix A
%   will be undone by the information supplied by vector c and setting
%   bUndo to true. The values in the added frames are set to zero.
%
%   X = ZEALOUSCROP(A,c, v, bUndo) the cropping of the cropped matrix A
%   will be undone by the information supplied by vector c and setting
%   bUndo to true. The values in the added frames are set to the value
%   given by v.
%
%   X = ZEALOUSCROP(A,c, v, bUndo,true) ensures that the result will have
%   odd size in all directions.
%
%   [X c] = ZEALOUSCROP(...) also returns a vector c that contains cropping
%   information and may be used in successive calls to obtain equal
%   results.


%   Function adapted (and debugged) from BW Lehr by F Schweser
%   2009/07/10
%
%
%   Changelog:
%   v2.0 - 2009/09/10 - F Schweser - Added frameValue argument.
%   v2.1 - 2010/02/15 - F Schweser - Input logical --> Output logical
%   v2.2 - 2010/02/22 - A Deistung - Undo of cropping is now added
%   v2.3 - 2010/02/22 - A Deistung - Undo cropping region can be defined by
%                                    a user defined value
%   v2.4 - 2010/08/19 - F Schweser - Bugfix: cropArray supplied and returned
%
%   v2.5 - 2010/10/05 - F Schweser - Now works with mids
%   v2.6 - 2011/03/10 - A Deistung - BugFix: in each diretion always the 
%                                    same values are now removed 
%   v2.7 - 2011/03/14 - F Schweser - Output deactivated.
%   v2.8 - 2011/03/30 - F Schweser - Bugfix: Did not work if there was
%                                    nothing to crop away in first dimension
%   v2.9 - 2011/04/04 - F Schweser - Some documentation added + Bugfix:
%                                    output dimensions were wrong
%   v3.0 - 2011/06/28 - D Lopez    - Bug Fix for 2D dimensions
%   v3.1 - 2011/08/08 - K Sommer   - bugfix of v3.0: input object is now left unaltered
%   v3.2 - 2018/09/18 - F Schweser - New option makeOdd



%% check input arguments
if nargin < 1 || isempty(ElementObj),      error('Not enough input arguments.');                       end


if isobject(ElementObj)
    Element = ElementObj.clone;
    Element = Element.img;
else
    Element = ElementObj;
end


if islogical(Element)
    isInputLogical = true;
else
    isInputLogical = false;
end

if nargin < 4 || isempty(bUndo)
    bUndo = false;
end
if nargin < 5 || isempty(makeOdd)
    makeOdd = false;
end

if nargin < 2 || isempty(cropArray)
    
    if nargin < 3 || isempty(frameValue)
        frameValue = [];
    end
    
    %% init
    cropValue = [];
    %Test = @(Elements)(isUnique(Elements, cropValue));
    
    dimensions = ndims(Element);
    
    first = zeros(dimensions, 1);
    last  = zeros(dimensions, 1);
    lastbeforecropping = zeros(dimensions, 1);
    sises = size(Element);
    lastbeforecropping(:) = sises;
    %output = sprintf('return Value: %s(', inputname(1));
    
    
    %% main part
    for dim = 1 : dimensions
        
        % find first row with non-unique entries
        for i = 1 : sises(1)
            %     if ~Test(Element(i, :), cropValue), break;  end
            if ~isUnique(Element(i, :), cropValue), break;  end
        end
        first(dim) = i;
        
        
        % automatically determine cropValue from this first row
        if dim == 1
            if i ~= 1 % first row has unique values
                cropValue = unique(Element(i-1, :));
            else % otherwise, check last rows bottom
                cropValue = unique(Element(sises(1), :));
            end
        end
        
        % determine last row with non-unique entries
        for i = sises(1) : -1 : first(dim)
            if ~isUnique(Element(i, :), cropValue), break;  end
        end
        last(dim) = i;

        if makeOdd
            if parity(last(dim) - first(dim))
                if last(dim) < lastbeforecropping(dim)
                    last(dim) = last(dim) + 1;
                else
                    first(dim) = first(dim) - 1;
                end
            end
        end
        
        % crop
        Element = Element(first(dim):last(dim), :);
        sises(1) = last(dim) - first(dim) + 1;
        Element = reshape(Element, sises);

        Element = shiftdim(Element, 1);
        sises = circshift(sises, [0 -1]);
        
        
    end
    
    if nargout > 1
        CropArray = [first, last, lastbeforecropping];
    else
        %display(sprintf('%s)', output(1:numel(output)-2)));
    end
    
    
    
    
elseif ~bUndo
    % cropArray supplied and bUndo is false
    if ndims(Element) == 4
        ElementOut = zeros(cropArray(1,2)-cropArray(1,1)+1,cropArray(2,2)-cropArray(2,1)+1,cropArray(3,2)-cropArray(3,1)+1,size(Element,4));
        for jDim = 1:size(Element,4)
            ElementOut(:,:,:,jDim) = Element(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2),cropArray(3,1):cropArray(3,2),jDim);
        end
        Element = ElementOut;
    elseif ndims(Element) == 3
        Element = Element(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2),cropArray(3,1):cropArray(3,2));
    elseif ndims(Element) == 2
        Element = Element(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2));
    else
        error('This number of dimensions is currently not supported.')
    end
    CropArray = cropArray;
    
else
    % cropArray supplied and bUndo is true
    if isempty(frameValue)
        frameValue = 0;
    end
    
    if ndims(Element) == 4
        ElementOut = zeros([cropArray(:,3)', size(Element, 4)]) + frameValue;
        ElementOut(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2),cropArray(3,1):cropArray(3,2),:) = Element;
        
    elseif ndims(Element) == 3
        ElementOut = zeros(cropArray(:,3)')  + frameValue;
        ElementOut(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2),cropArray(3,1):cropArray(3,2)) = Element;
    elseif ndims(Element) == 2
        ElementOut = zeros(cropArray(:,3)')  + frameValue;
        %ElementOut = zeros(cropArray(:,2)')  + frameValue;
        ElementOut(cropArray(1,1):cropArray(1,2),cropArray(2,1):cropArray(2,2)) = Element;
    else
        error('This number of dimensions is currently not supported.')
    end
    
    Element = ElementOut;
end



if isInputLogical
    Element = logical(Element);
end


if isobject(ElementObj)
    ElementExp = ElementObj.clone;
    ElementExp.img = Element;
else
    ElementExp = Element;
end



%% member functions
    function isTrue = isUnique(Element, cropValue)
        uniqueVector = unique(Element);
        
        isTrue = numel(uniqueVector) == 1;
        
        if isTrue
            if uniqueVector ~= frameValue
                isTrue = false;
            end
            if uniqueVector ~= cropValue
                isTrue = false;
            end
        end
    end

end



% function isTrue = isUniqueValue(Element, value)
% isTrue = isUnique(Element) & (unique(Element) == value);
% end