#!/bin/bash


######################################
# Available variables in this script:
#
#  WORKDIR - current working dir
#  EXID    - exam ID
#  JOBSTR  - job identifier string
#  All variables defined in dev_env.sh
#
# Must define:
#      
#  JOBNAMES, COMMANDS
#
######################################

unset JOBNAMES
unset COMMANDS

JOBNAMES+=(inversion_lsqr)
COMMANDS+=("${GIT_APP}/lsqr/inversion_lsqr.slurm \
                ${GIT_PL} \
                ${JOBSTR}_EX${EXID} \
                ${WORKDIR}/gradunwarp.mat \
                ${WORKDIR}/background_correction_sharp.mat \
                ${WORKDIR}/create_masks.mat \
                ${WORKDIR}/inversion_lsqr.mat \
                ${WORKDIR}\
                ${WORKDIR}/inversion_lsqr.nii.gz")

JOBNAMES+=(inversion_heidi)
COMMANDS+=("${GIT_APP}/heidi/inversion_heidi.slurm \
                ${GIT_PL} \
                ${JOBSTR}_EX${EXID} \
                ${WORKDIR}/gradunwarp.mat \
                ${WORKDIR}/background_correction_sharp.mat \
                ${WORKDIR}/create_masks.mat \
                ${WORKDIR}/inversion_lsqr.mat \
                ${WORKDIR}\
                ${WORKDIR}/inversion_heidi.mat \
                ${WORKDIR}/inversion_heidi.nii.gz")