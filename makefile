
#### Set SLURM_FCT_REPO variable to the slurm-functions repository path
SLURM_FCT_REPO := /projects/academic/schweser/mbl/R01NS114227/qsm_reconstruction/git/pi4s

#### Set SLURM_LOG variable to the central slurm logfile directory
SLURM_LOG := /projects/academic/schweser/mbl/R01NS114227/qsm_reconstruction/slurm

#### Some definitions about pipeline
THISFILEDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CMD := $(SLURM_FCT_REPO)/submit_pl.sh $(THISFILEDIR) $(SLURM_LOG)
ifndef VERBOSE
.SILENT:
endif

#### Expected input and putput files of the pipeline
gradunwarp.mat background_correction_sharp.mat create_masks.mat:
	echo "Start."

output_LSQR/inversion_lsqr.nii.gz output_LSQR/inversion_lsqr.mat : gradunwarp.mat background_correction_sharp.mat create_masks.mat
	$(CMD) 0 0 general-compute

output_HEIDI/inversion_heidi.nii.gz output_HEIDI/inversion_heidi.mat : gradunwarp.mat background_correction_sharp.mat create_masks.mat output_LSQR/inversion_lsqr.mat
	$(CMD) 1 0 general-compute

qsm : output_HEIDI/inversion_heidi.nii.gz output_HEIDI/inversion_heidi.mat
	echo Done.
