function dataArray = zeropadding(dataArray,sizeVector,paddingValue)

%ZEROPADDING Zero-pads an N-D data array symmetrically.
%
%   ZEROPADDING Padds an Array with zeros symmetrically.
%
%
%   Syntax
%
%   ZEROPADDING(X,s)
%   ZEROPADDING(X,s,a)
%
%
%   Description
%
%   ZEROPADDING(X,s) padds N-D array X with zeros so that size of X is s (vector).
%   Note: Both, X and s must be even or odd.
%
%   ZEROPADDING(X,s,a) padds with a instead of zeros.
%
%
%   (Function supports in-place calls using X)
%
%   See also: MAKEEVEN

% 02/2009 - F Schweser, mail@ferdinand-schweser.de
%
%   Changelog:
%
%   2009/09/22 - F Schweser - Error message and documentation changed.


%% check input arguments
if nargin < 2
    error('This function requires at least two input parameters.')
end
if nargin < 3 || isempty(paddingValue)
    paddingValue = 0;
end


%% main part
dataArraySizeReduced = size(dataArray);
if size(dataArraySizeReduced(dataArraySizeReduced ~= 1),2) ~= size(sizeVector(sizeVector ~= 1),2)
    error('Vector of desired matrix size has wrong size.')
else
    sizeDiff = sizeVector(1:ndims(dataArray)) - size(dataArray);
end

if (sizeDiff >= 0) ~= ones(size(dataArraySizeReduced))
    error('Desired matrix size is smaller than size of input-data. No padding possible!')
else
    
    
    if norm((sizeDiff/2) - round(sizeDiff/2)) ~= 0
        error('Both, size of input-data array and desired size must be even or odd.')
    else
        dataArray = padarray(dataArray,sizeDiff/2,paddingValue);
    end
end