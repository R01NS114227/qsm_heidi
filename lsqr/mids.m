classdef mids < handle
    
    % MRI Image Data Set (MIDS) Class
    %
    % Supported properties
    %
    % img            - n-dimensional volume of the complex MRI data
    %
    % hdrDCM         - cell array of the dcm-headers. if the data is
    %                  complex theh the dcm-headers are taken from the magnitude series.
    %
    %
    % sourceFileInfo - additional information to the imported data.
    %                  This property has different fields for nifti and
    %                  DICOM import. The common fields are:
    %   # pathMagnitude - string that defines the path
    %                    were the inpuet magnitude data (DICOM images (i.e., series path)
    %                    or nifti-file) is located.
    %   # pathPhase    - string that defines the path
    %                    were the input phase data (DICOM images (i.e., series path)
    %                    or nifti-file) is located.
    %
    % processingFileInfo - defines additional information that modifies the
    %                      imported mids object. The following fields are
    %                      supported.
    %   # projectionLength - defines the number of slices of projected
    %                        slices. If projectionLength is 1 no projetion
    %                        is performed.
    %
    %                  *nifti source file information with following fields
    %   # filetype
    %   # fileprefix
    %   # machine
    %                  *DICOM source file information with following
    %                  fields
    %
    % Supported methods
    %
    % # getphase
    % # gethdrDCM      - returns the hdrDCM property
    % # getfirsthdrDCM - reveals the header of the first hdrDCM
    %
    % # getvolume      - returns a mids-object with the selected 4th dimension volume(s)
    %                    of a N-D dataset and also adjusts the dcm-header
    %                    information accordingly.
    %                    This method supports the following inputs:
    %                    * 1D array with volume numbers (e.g. [1,5,8])
    %                    * strings with the keywords:
    %                    ** 'even' --> extracts volumes with even numbers
    %                    ** 'odd'  --> extracts volumes with odd numbers
    %                    ** 'all'  --> extracts all volumes  %
    %                    ** 'end'  --> extracts last volume
    %                    ** '2toend'  --> extracts all the volumes 2 to end
    %
    % # getvoxelsize   - returns voxels size of the data set
    %                    voxelsize = obj.getvoxelsize; 
    %                    returns also the original voxel size: 
    %                    voxelsize = obj.getvoxelsize(isoriginal); --> here
    %                    isoriginal is a boolean that determines if the
    %                    original voxel size should be returned 
    %
    % #
    %
    % # setvoxelsize   - sets the voxel size of the data set
    %                    obj = obj.setvoxelsize([0.5, 0.5, 2]);
    %
    % # getechoTimes   - returns a 1D array with the echo times of the data
    %                    set. If no echo times can be extracted the
    %                    result is empty.
    %                    echoTimes = obj.getEchoTimes;
    % # getOrientation(regMatrix) - returns orientation struct. regMatrix is an optional
    %			            registration matrix (if registration has been performed to dataset)
    %
    % # setechoTimes   - sets the echo times of the data set using a 1D
    %                    array representing echo times.
    %                    obj = obj.setEchoTimes([0, 8, 15]);
    % # getMagneticFieldStrength - returns magnetic field strengths in Tesla
    %
    % # setMagneticFieldStrength - sets magnetic field strengths in Tesla
    %
    % # dump            - saves whole mids object to disk and sets img property
    %                     to empty
    %
    % # dedump          - loads dumped mids object from dsik
    %
    % # setImportRoutine - the import routines that are supported are
    %                      listed in flipimporteddata.
    %
    % # setImportHeaderType - string that defines the header type of the
    %                         imported data. Currently, only 'NIFTI', 'DICOM', 'BRUKER', and 'ICERAW' is supported.
    %
    % # setEchoNumbers  - this method sets the echo numbers in the DICOM
    %                     header in accordance with the determined echo times.
    %
    % # setOrientation - this method sets the rotation structure. The
    %                    rotation structure is given by: 
    %                    rotation.matrix
    %                    rotation.axis
    %                    rotation.angle
    %
    % # getObjectOrientation/setObjectOrientation - sets and gets the
    %                     object orientation in the scanner (e.g.
    %                     supine-head first: 'SHF','HFS' (sphinx))
    %
    % # getMatrixOrientation/setMatrixOrientation - sets and gets the data
    %                     matrix orientation type: 'transversal',
    %                     'coronar', or 'sagittal'
    %
    %
    % # check(type,level)  - performs data check. The following types are
    %                        supported:
    %                            rawQSMInput
    %                               level: relaxed, strict
    %
    %
    % 2010/09/06 - F Schweser - mail@ferdinand-schweser.de
    %
    %   Changelog:
    %
    %   2010/09/06 - v1.0 - F Schweser - new.
    %   2010/12/06 - v1.1 - A Deistung - help extended, the following
    %                                    methods were introduced
    %                                    getfirshdrDCM,
    %                                    getvoxelsize, setvoxelsize
    %                                    gethdrDCM, getprojectionLength
    %   2010/12/23 - v1.2 - A Deistung - getvolume now copies also the
    %                                    corresponging dcm-headers in the
    %                                    new mids-object
    %
    %
    %   2011/20/01 - v1.4 - A Deistung - bugfix getvolume --> checking
    %                                    software version
    %   2011/24/01 - v1.41- K Sommer   - bugfix gevolume --> check in
    %                                    software version (fieldnames)
    %   2011/24/01 - v1.5 - A Deistung - methods setEchoTimes and
    %                                    getEchoTimes were added.
    %   2011/14/03 - v1.6 - F Schweser - class type changed to handle class
    %                                    + set method for img added
    %   2011/31/03 - v1.7 - F Schweser - method getOrientation added + setImportRoutine
    %   2011/18/04 - v1.8 - F Schweser - coordinateSystem property added +
    %                                    getSequenceInformation added
    %   2011/31/05 - v1.9 - A Deistung - the method getvolume was extended
    %                                    to accecpt an 1D array of volume numbers
    %                                    as well as the strings 'odd',
    %                                    'even', and 'all'
    %   2011/06/10 - v2.0 - A Deistung - Bugfix: getvolume --> dicom header
    %                                    selection
    %   2011/06/10 - v2.1 - A Deistung - the methods set + getMagneticField
    %                                    Strength were added
    %   2011/06/20 - v2.2 - A Deistung - Bugfix: getvolume for VA-systems
    %	2011/07/12 - v2.3 - F Schweser - getOrientation now features registration matrix input
    %	2011/07/15 - v2.4 - A Deistung - Bugfix: getvolume for VA-systems
    %	2011/07/19 - v2.4 - F Schweser - Improved error handling for getOrientation
    %   2011/08/01 - v2.5 - F Schweser - Bugfix: rotationfromniftiheader does not
    %                                    support regMatrix!
    %   2011/08/15 - v2.6 - A Deistung - since sometimes the dcm-header can
    %                                    be cellstructure instead of an array structure,
    %                                    therefore try-catch commands were included to
    %                                    catch this case
    %   2011/08/16 - v2.61 - A Deistung - if the DICOM header from a
    %                                     specific file is requested, it is now ensured
    %                                     that the output data is a struct
    %   2011/09/27 - v2.62 - A Deistung - Check in getEchoTimes: if echo numbers do
    %                                     not correspond to the number of echo times.
    %   2011/10/20 - v2.7 - F Schweser - Dump and Dedump features added
    %   2011/11/11 - v2.7.1- A Deistung - help extended
    %
    %   2012/01/17 - v2.8- F Schweser - New member function
    %                                   "getImportHeaderType" and new
    %                                   importHeaderType = 'none' for
    %                                   numerical array data
    %   2012/01/18 - v2.9- F Schweser - Now .getEchoTimes supports number
    %                                    of echo specification
    %   2012/01/126 - v3.0- F Schweser - Now .getvolume also changes echo
    %                                   times field
    %   2012/02/01 - v3.1- F Schweser - Removed the "extracting volume"
    %                                   stuff - too annoying
    %   2012/02/08 - v3.2- F Schweser - Bugfix: getvolume did not work if
    %                                   no echo time was assigned.
    %   2012/04/05 - v3.3- A Deistung - the method getvolume now accepts the value 'end';
    %                                   BugFix: getHeader if no
    %                                   CoordinateSystemType is
    %                                   specified. In this case the
    %                                   result is empty
    %   2012/05/10 - v3.31- A Deistung - the method getvolume now accepts the value '2toend';
    %   2012/07/15 - v3.4- F Schweser - Bugfix: mriconvert-importeted data
    %                                   did not allow getvolume()
    %   2012/08/02 - v3.5- A Deistung - the method setEchoNumbers was
    %                                   introduced in order to account with
    %                                   DICOM data with incorrect echoNumbers
    %   2012/08/27 - v3.6- F Schweser - new member functions check,
    %                                   setMatrixOrientation,
    %                                   getMatrixOrientation,
    %                                   getMatrixOrientationChanged, setMatrixOrientationChanged
    %                                   + bugfix: EchoNumber
    %   2012/09/03 - v3.7 - F Schweser - new member functions
    %                                   getObjectOrientation and setObjectOrientation
    %   2012/09/04 - v3.71 - F Schweser - now .check does not require DICOM
    %                                   header
    %   2012/09/10 - v3.8 - F Schweser - added HFS (sphinx) object position
    %   2012/11/13 - v3.81 - F Schweser - minor bugfix regarding the letter
    %                                    order of HFP in setObjectOrientation
    %   2012/10/02 - v3.9 - A Deistung - ICERAW header is now supported
    %   2012/10/26 - v4   - A Deistung - a new property: voxelsizeOriginal
    %                                    was introduced
    %   2012/10/10 - v4.1 - A Deistung - the method setOrientation was
    %                                    added
    %   2013/04/01 - v4.2 - F Schweser - No April-joke: Changed getEchoTimes
    %   2013/11/20 - v4.3 - A Deistung - bugfix: Bruker orientation information                             
    %   2013/11/20 - v4.4 - F Schweser - Improved functionality for
    %                                    getvolume: Now returns
    %                                    4th-dimension volume even if array
    %                                    dimension is >4.
    %   2014/03/03 - v4.5 - F Schweser - Now checks for spatial
    %                                    interpolation in check() with
    %                                    strict mode
    %   2014/03/31 - v4.6 - F Schweser - Check of rotation matrix + now
    %                                    updates axis and angle in setOrientation 
    %                                    +  Now checks for reflections in the
    %                                   orientation matrix. The orientation
    %                                   matrix can contain reflections, which
    %                                   makes it "improper". In this case the
    %                                   axis/angle calculations are incorrect
    %                                   using vrrotmat2vec!!! I implemented a
    %                                   comprehensive checking of this and an
    %                                   automated compensation using a new
    %                                   coordinate system called
    %                                   reflectionCorrected.
    %  2014/04/10 - v4.7 - F Schweser - Bugfix for Philips data: Introduced
    %                                   strtrm to setObjectOrientation.
    %
    %  2014/06/30 - v4.8 - F Schweser - Changed bruker coordinate system switches
    %  2014/07/17 - v4.9 - F Schweser - Changed spatial interpolation
    %                                   detection (check)
    %  2014/12/08 - v5.0 - F Schweser - Bugfix in getvolume, which
    %                                   prevented use of 'odd' and 'even'
    %  2015/07/15 - v5.1 - F Schweser - added ROTATIONFROMBRUKERHEADER
    %  2016/05/23 - v5.2 - F Schweser - Bugfix regarding orientations for improper rotation with RAW format  
    %  2016/12/13 - v5.3 - F Schweser - Bugfix: Unique sorts entries, although may not be sorted
    %  2020/10/05 - v5.4 - F Schweser - Bugfix: Echo time sorting for data from vienna
    %
    %
    
    
    %
    %%
    
    
    properties
        img                 % image data block
        importRoutine       % nifti writing routine
        sourceFileInfo      % nifti source file information
        processingFileInfo  % processing information of the current img-object
        diskDumpFile        % filename of dump file on disk
    end
    
    properties (Hidden = true, GetAccess = private, SetAccess = private)
        voxelsize           % voxel size of the image data block
        voxelsizeOriginal   % original voxel size of the image data block from import
        echoTimes           % echo times of the image data block
        magneticFieldStrength% magnetic field strength of the image data block
        hdr                 % nifti header
        hdrDcm              % DICOM header
        hdrBruker           % Bruker header
        hdrIceRaw           % ICE header
        hdrInformal         % nifti header (informal)
        hdrDcmInformal      % DICOM header (informal)
        hdrBrukerInformal   % Bruker header (informal)
        hdrIceRawInformal   % ICE header (informal)
        hdrRaw              % reserved for arbitrary raw data import (e.g. Toshiba raw data import)
        coordinateSystem    % type of coordinate system
        importHeaderType    % specifies which header is the correct header for the data array
        matrixOrientation   % matrix orientation (what is z-axis? transversal/coronal/sagittal)
        matrixOrientationOriginal; % original matrix orientation from import
        objectOrientation   % orientation of the object in the scanner
        Rotation            % rotation structure
        reflectionDims      % boolean vector that indicates flips required to obtain a proper orientation matrix (i.e. det(matrix) = 1)
        coordinateSystemTypeBeforeReflectionCorrection

    end
    
    methods
        
        
        %% get methods
        function obj = mids()
            % class constructor
        end
        
        function set.img(this,value)
            this.img = value;
        end
        
        
        function newObj = clone(this)
            % create an empty instance of the class
            newObj = eval(class(this));
            
            % copy non-dependent properties, by creating a meta object for
            % the class of 'this'
            mo = eval(['?',class(this)]);
            ndp = findobj([mo.Properties{:}],'Dependent',false);
            for idx = 1:length(ndp)
                newObj.(ndp(idx).Name) = this.(ndp(idx).Name);
            end
        end
        
        function objNew = getphase(this)
            % writes phase to img property
            objNew = clone(this);
            objNew.img = angle(objNew.img);
        end
        
        function objNew = getmagnitude(this)
            % writes magnitude to img property
            objNew = clone(this);
            objNew.img = abs(objNew.img);
        end
        
        function objNew = getvolume(this,volumeNo)
            % selects one volume from a 4-D dataset. also adjust the
            % dcm-header information
            % the following values are allowed for volumeNo
            % * 1D array with volume numbers that should be extracted
            % * string with the following characters:
            %     'even' --> extracts even volume numbers
            %     'odd'  --> extracts odd volume numbers
            
            objNew = clone(this);
            if ischar(volumeNo)
                numberOfVolumes = size(objNew.img,4);
                switch volumeNo
                    case 'odd'
                        volumeNo = (1:2:numberOfVolumes);
                    case 'even'
                        volumeNo = (2:2:numberOfVolumes);
                    case 'all'
                        volumeNo = (1:numberOfVolumes);
                    case 'end'
                        volumeNo = numberOfVolumes;
                    case '2toend'
                        volumeNo = (2:numberOfVolumes);
                    otherwise
                        error('You have passed a string with a keyword that is not supported by this method. If you pass a string only the words "odd", "even" and "all" are allowed. Otherwise you can also pass an 1D array of volume numbers.')
                end
            end
            %disp(['Extracting volume number(s): ', num2str(volumeNo)]);
            objNew.img = objNew.img(:,:,:,volumeNo,:,:,:,:,:,:,:,:);
            
            %numberOfEchoes = length(objNew.getEchoTimes);
            objNew.setEchoTimes(this.getEchoTimes(volumeNo));
            
            headerTmp = objNew.getHeader('DICOM');
            if iscell(headerTmp)
                % header can sometimes be cell
                headerTmp = headerTmp{1};
            end
            if ~isempty(headerTmp)
                
                
                tmp = objNew.getHeader('DICOM',1);
                if ~iscell(tmp)
                    % if headerarray is cell (mriconvert)--> does not
                    % work!)
                    isSkipHeaderReplacement = false;
                    if isfield(tmp, 'EchoNumber')
                        try
                            echoNumbers = arrayfun(@(x)x.EchoNumber, objNew.getHeader('DICOM'));
                        catch
                            echoNumbers = cellfun(@(x)x.EchoNumber, objNew.getHeader('DICOM'));
                        end
                    elseif isfield(tmp, 'EchoNumbers')
                        try
                            echoNumbers = arrayfun(@(x)x.EchoNumbers, objNew.getHeader('DICOM'));
                        catch
                            echoNumbers = cellfun(@(x)x.EchoNumbers, objNew.getHeader('DICOM'));
                        end
                    else
                        dispstatus('This DICOM header does not contain echo time information. Skipping header replacement in volume selection!',[],'warning')
                        isSkipHeaderReplacement = true;
                    end
                    
                    if ~isSkipHeaderReplacement
                        echoNumbers = unique(echoNumbers);

                        VAVersionCorrection = 0;

                        if isfield(objNew.getHeader('DICOM',1), 'SoftwareVersion')
                            if ~isempty(strfind(objNew.getHeader('DICOM',1).SoftwareVersion, 'VA'))  %correct volume number for VA - Systems
                                echoNumbers = echoNumbers + 1;
                                VAVersionCorrection = 1;
                            end
                        elseif isfield(objNew.getHeader('DICOM',1), 'SoftwareVersions')
                            if ~isempty(strfind(objNew.getHeader('DICOM',1).SoftwareVersions, 'VA'))  %correct volume number for VA - Systems
                                echoNumbers = echoNumbers + 1;
                                VAVersionCorrection = 1;
                            end
                        end

                        if numel(echoNumbers) < numel(volumeNo) || numel(echoNumbers) < max(volumeNo)
                            % this can happen if the DICOM header is not
                            % absolutely correct. E.g. if data was de.mosaiced
                            % (Lausanne)
                            warning('The DICOM header seems to be corrupted. The echo numbers are now guessed. Unpredictable behaviour may result!')
                            echoNumbers = 1:max(volumeNo);
                        end

                        echoidx = false(1, length(objNew.getHeader('DICOM')));
                        for iVols=1:length(volumeNo)
                            if isfield(tmp, 'EchoNumber')
                                try
                                    echoidx_curVolidx = arrayfun(@(x)isequal(x.EchoNumber,echoNumbers(volumeNo(iVols))-VAVersionCorrection), objNew.getHeader('DICOM'));
                                catch
                                    echoidx_curVolidx = cellfun(@(x)isequal(x.EchoNumber,echoNumbers(volumeNo(iVols))-VAVersionCorrection), objNew.getHeader('DICOM'));
                                end
                            elseif isfield(tmp, 'EchoNumbers')
                                try
                                    echoidx_curVolidx = arrayfun(@(x)isequal(x.EchoNumbers,echoNumbers(volumeNo(iVols))-VAVersionCorrection), objNew.getHeader('DICOM'));
                                catch
                                    echoidx_curVolidx = cellfun(@(x)isequal(x.EchoNumbers,echoNumbers(volumeNo(iVols))-VAVersionCorrection), objNew.getHeader('DICOM'));
                                end
                            end
                            echoidx = echoidx | echoidx_curVolidx;

                        end
                        if sum(echoidx(:)) ~= 0
                            objNew.hdrDcm = objNew.getHeader('DICOM',echoidx);
                        end
                    end
                end
            else
                warning('Skipping header replacement in volume selection!');
            end
        end
        
        
        function HeaderVar = getHeader(obj,type,headerNo)
            % returns header
            if nargin < 2 || isempty(type)
                %return header of current coordinate system
                type = obj.getCoordinateSystemType;
                if isempty(type)
                    type = 'UNDEFINED';
                end
            end
            
            if nargin < 3 || isempty(headerNo)
                switch upper(type)
                    case 'DICOM'
                        HeaderVar = obj.hdrDcm;
                    case 'NIFTI'
                        HeaderVar = obj.hdr;
                    case 'BRUKER'
                        HeaderVar = obj.hdrBruker;
                    case 'ICERAW'
                        HeaderVar = obj.hdrIceRaw;
                    case 'RAW'
                        HeaderVar = obj.hdrRaw;
                    case 'UNDEFINED'
                        HeaderVar = [];
                    case 'REFLECTIONCORRECTED'
                        error('No header can be returned for coordinate system type reflectionCorrected.')
                    otherwise
                        error('This header type is not supported.')
                end
            else
                switch type
                    case 'DICOM'
                        HeaderVar = obj.hdrDcm(headerNo);
                        if (length(headerNo) == 1) && iscell(HeaderVar)
                            HeaderVar = HeaderVar{:};
                        end
                    case 'NIFTI'
                        HeaderVar = obj.hdr(headerNo);
                    case 'BRUKER'
                        HeaderVar = obj.hdrBruker(headerNo);
                    case 'ICERAW'
                        HeaderVar = obj.hdrIceRaw(headerNo);
                    otherwise
                        error('Specified header type not supported.')
                end
            end
            
        end
        
        function HeaderVar = getInformalHeader(obj,type,headerNo)
            % returns informal header (does not necessarily match data)
            if nargin < 2 || isempty(type)
                %return header of current coordinate system
                type = obj.getCoordinateSystemType;
                if isempty(type)
                    type = 'UNDEFINED';
                end
            end
            
            if nargin < 3 || isempty(headerNo)
                switch upper(type)
                    case 'DICOM'
                        HeaderVar = obj.hdrDcmInformal;
                    case 'NIFTI'
                        HeaderVar = obj.hdrInformal;
                    case 'BRUKER'
                        HeaderVar = obj.hdrBrukerInformal;
                    case 'ICERAW'
                        HeaderVar = obj.hdrIceRawInformal;
                    case 'UNDEFINED'
                        HeaderVar = [];
                    case 'REFLECTIONCORRECTED'
                        error('No header can be returned for coordinate system type reflectionCorrected.')
                    otherwise
                        error('This header type is not supported.')
                end
            else
                switch type
                    case 'DICOM'
                        HeaderVar = obj.hdrDcmInformal(headerNo);
                        if (length(headerNo) == 1) && iscell(HeaderVar)
                            HeaderVar = HeaderVar{:};
                        end
                    case 'NIFTI'
                        HeaderVar = obj.hdrInformal(headerNo);
                    case 'BRUKER'
                        HeaderVar = obj.hdrBrukerInformal(headerNo);
                    case 'ICERAW'
                        HeaderVar = obj.hdrIceRawInformal(headerNo);
                    otherwise
                        error('Specified header type not supported.')
                end
            end
            
        end
        
        
        function voxelsize = getvoxelsize(obj, isoriginal)
            % returns voxels size of the data set   
            % isoriginal --> flag that determines if the original voxel
            % size should be returned
            
            if nargin < 2
                isoriginal = false;
            end
            if isempty(obj.voxelsize)
                if ~isempty(obj.getHeader('NIFTI'))
                    voxelsize = obj.getHeader('NIFTI').dime.pixdim(2:4);
                elseif ~isempty(obj.getHeader('DICOM'))
                    voxelsize = [obj.getHeader('DICOM',1).PixelSpacing(1), obj.getHeader('DICOM',1).PixelSpacing(2), obj.getHeader('DICOM',1).SliceThickness];
                elseif ~isempty(obj.getHeader('BRUKER'))
                    if numel(obj.getHeader('BRUKER').method.PVM_SpatResol) == 3
                        voxelsize = obj.getHeader('BRUKER').method.PVM_SpatResol';
                    else
                        voxelsize = [obj.getHeader('BRUKER').method.PVM_SpatResol',obj.getHeader('BRUKER').method.PVM_SliceThick]; % in mm
                    end
                elseif ~isempty(obj.getHeader('ICERAW'))
                    prot = obj.getHeader('ICERAW');
                    voxelsize = [prot.dReadoutFOV./prot.NImageCols, prot.dPhaseFOV./prot.NImageLins, prot.dThickness./prot.lPartitions];
                else
                    warning('Matlab:mids', 'There is no voxel size specified in this mids object. Setting the voxel size to [1,1,1]');
                    voxelsize  = [1, 1, 1];
                end
                obj.voxelsizeOriginal = voxelsize;
            elseif isoriginal
                voxelsize = obj.voxelsizeOriginal;
            else
                voxelsize = obj.voxelsize;
            end
        end
        
        
        
        
        
        function orientation = getObjectOrientation(this)
            if isempty(this.objectOrientation);
                error('Object orientation unset. Please use setObjectOrientation.')
            else
                orientation = this.objectOrientation;
            end
        end
        
        
        function orientation = getMatrixOrientation(this)
            %%% returns/extracts the data matrix orientation, i.e.
            %%% transversal/sagittal/coronal
            % header interpretation empirically found by script in
            % swi-susceptibiltiy-mapping/scripts/dicom_header_analsyis
            
            if ~isempty(this.matrixOrientation);
                orientation = this.matrixOrientation;
            else
                
                fieldDirection = abs(this.getOrientation.matrix * [0 0 1]');
                
                [maxB0Proection,maxB0ProectionAxis] = max(fieldDirection);
                 
                switch strtrim(sort(this.getObjectOrientation))
                    case {'FHS','FHP'}
                        if maxB0ProectionAxis == 3
                            orientation = 'transversal';
                        elseif maxB0ProectionAxis == 1
                            orientation = 'sagittal';
                        else
                        
                            % in the case of HFS object orientation the head is
                            % sometimes tilted a bit so that the slice samples the
                            % brain in a normalized way. Therefore, we need to be
                            % care when deciding the slice orientation
                            if fieldDirection(2) > fieldDirection(3)*2 % empirical threshold
                                orientation = 'coronal';
                            else
                                orientation = 'transversal';
                            end
                            
                        end
                        
                        
                    case 'hinpsx' % sphinx
                        if maxB0ProectionAxis == 2
                            orientation = 'transversal';
                        else
                            error('This matrix orientation is not yet implemented for sphinx orientation.')
                        end
                    otherwise
                        error(['Object orientation ' this.getObjectOrientation  ' not yet supported.'])
                end
                
                % try to get it from the header for comparison
                header = this.getHeader('DICOM');
                if ~isempty(header) && isfield(header(1),'Private_0051_100d'); % may be empty in the case of NII source
                    firstSlicePosition = header(1).Private_0051_100d;
                    lastSlicePosition = header(end).Private_0051_100d;
                    
                    if strcmp(firstSlicePosition(4),'H') || strcmp(lastSlicePosition(4),'H') || ...
                            strcmp(firstSlicePosition(4),'F') || strcmp(lastSlicePosition(4),'F')
                        orientationHeader = 'transversal';
                    elseif strcmp(firstSlicePosition(4),'R') || strcmp(lastSlicePosition(4),'R') || ...
                            strcmp(firstSlicePosition(4),'L') || strcmp(lastSlicePosition(4),'L')
                        orientationHeader = 'sagittal';
                    elseif strcmp(firstSlicePosition(4),'P') || strcmp(lastSlicePosition(4),'P') || ...
                            strcmp(firstSlicePosition(4),'A') || strcmp(lastSlicePosition(4),'A')
                        orientationHeader = 'coronal';
                    else
                        warning('Matrix orientation cannot be extracted from DICOM header Private_0051_100d: Content cannot be interpreted.')
                    end
                    
                    
                    if ~strcmp(orientationHeader,orientation)
                        warning(['DICOM header Private_0051_100d shows different slice orientation (' orientationHeader ') from the one detected here (' orientation '; projection: ' num2str(maxB0Proection*100) '%). The DICOM-information is discarded...'])
                    end
                end
            end
            this.setMatrixOrientation(orientation);
        end
        
        
        function orientation = getMatrixOrientationOriginal(this)
            orientation = this.matrixOrientationOriginal;
        end
        
        function magneticFieldStrength = getMagneticFieldStrength(obj)
            % returns voxels size of the data set
            if isempty(obj.magneticFieldStrength)
                if ~isempty(obj.getHeader('DICOM'))
                    magneticFieldStrength = obj.getHeader('DICOM',1).MagneticFieldStrength;
                elseif ~isempty(obj.getHeader('BRUKER'))
                    h = getconstants;
                    magneticFieldStrength = obj.getHeader('BRUKER').acqp.BF1/h.gyromagneticRatio*2*pi*1e6; % in T
                elseif ~isempty(obj.getHeader('ICERAW'))
                    magneticFieldStrength = obj.getHeader('ICERAW').flNominalB0;
                else
                    warning('Matlab:mids', 'There is no magnetic field strength specified in this mids object. Please set the magnetic field strength using the method setMagneticFieldStrength.');
                    magneticFieldStrength = [];
                end
            else
                magneticFieldStrength = obj.magneticFieldStrength;
            end
        end
        
        function Rotation = getOrientation(this,regMatrix)
            % returns Rotation struct of orientation information
            if nargin < 2
                regMatrix = [];
            end
            
            if ~isempty(this.matrixOrientationOriginal) && ~strcmp(this.matrixOrientation,this.matrixOrientationOriginal)
                error(['The orientation cannot be calculated because the data matrix orientation has been changed from ' this.matrixOrientationOriginal ' to ' this.matrixOrientation '. Use FLIPDATAMATRIX for restoring original orientation.'] )
            end
            
            if ~isempty(this.importHeaderType) 
                if isempty(this.Rotation)
                    switch this.importHeaderType
                        case 'DICOM'
                            switch this.getCoordinateSystemType
                                case 'DICOM'
                                    Rotation = rotationfromdcmheader(this.getHeader('DICOM'),regMatrix);
                                case 'reflectionCorrected'
                                    if strcmp(this.coordinateSystemTypeBeforeReflectionCorrection,'DICOM')
                                        Rotation = rotationfromdcmheader(this.getHeader('DICOM'),regMatrix);
                                    else
                                        error('This is reflectionCorrected coordinate system type that was not created from DICOM. For this reason header cannot be created from DICOM. Convert back to old system first.')
                                    end
                                otherwise 
                                    error('Orientation information not available for the current coordinate system type. Switch, e.g., to DICOM.')
                            end
                        case 'NIFTI'
                            if isempty(regMatrix)
                                if strcmp(this.getCoordinateSystemType,'reflectionCorrected') 
                                    if strcmp(this.coordinateSystemTypeBeforeReflectionCorrection,'NIFTI')
                                        Rotation = rotationfromniftiheader(this.getHeader('NIFTI'),'NIFTI');
                                    else
                                        error('This is reflectionCorrected coordinate system type that was not created from NIFTI. For this reason header cannot be created from NIFTI. Convert back to old system first.')
                                    end
                                else
                                    Rotation = rotationfromniftiheader(this.getHeader('NIFTI'),this.getCoordinateSystemType);
                                end
                            else
                                error('Registration matrix input not supported for NIFTI header.')
                            end
                        case 'BRUKER'
                            switch this.getCoordinateSystemType
                                case 'BRUKER'
                                    Rotation = rotationfrombrukerheader(this.getHeader('Bruker'),this.getCoordinateSystemType);
                                case 'reflectionCorrected'
                                    if strcmp(this.coordinateSystemTypeBeforeReflectionCorrection,'BRUKER')
                                        Rotation = rotationfrombrukerheader(this.getHeader('Bruker'),regMatrix);
                                    else
                                        error('This is reflectionCorrected coordinate system type that was not created from BRUKER. For this reason header cannot be created from FROM. Convert back to old system first.')
                                    end
                                otherwise
                                    error('This data is from BRUKER. Header conversion currently not supported.')
                            end
                        case 'ICERAW'
                            switch this.getCoordinateSystemType
                                case 'ICERAW'
                                    Rotation = this.getHeader('ICERAW').Orientation;
                                otherwise
                                    error('This data is ICERAW. Header conversion currently not supported.')
                            end
                        otherwise
                            error('importHeaderType property unknown or not set.')
                    end
                    
                    % if reflectionCorrected system type, we need to
                    % correct the reflection in the header here
                    if strcmp(this.getCoordinateSystemType,'reflectionCorrected')
                       
                        reflectionDimsTmp = double(this.getReflectionDims);
                        reflectionDimsTmp(reflectionDimsTmp == 1) = -1;
                        reflectionDimsTmp(reflectionDimsTmp == 0) = 1;
                        
                        newMatrix = Rotation.matrix * diag(reflectionDimsTmp);
                        
                        Rotation.translationVector = Rotation.translationVector .* reflectionDimsTmp;
                        Rotation.matrix = newMatrix;
                        Rotation.affine4x4Matrix = eye(4,4);
                        Rotation.affine4x4Matrix(1:3,1:3) = diag(this.getvoxelsize) * Rotation.matrix;
                        Rotation.affine4x4Matrix(1:3,4) = Rotation.translationVector;
                        OptsVRROT.epsilon = 1e-6;
                        vectorExpressionTmp = real(vrrotmat2vec(Rotation.matrix,OptsVRROT));
                        Rotation.axis  = vectorExpressionTmp(1:3);
                        Rotation.angle = rad2deg(vectorExpressionTmp(4));
                        Rotation.eulerAngles = rad2deg(orth2ang(Rotation.matrix));
                        Rotation.systemType = 'reflectionCorrected';
                    end
                    
                else
                    Rotation = this.Rotation;
                    warning('Matlab:mids', 'Rotation was set externally. Be sure that orientation information matches the data.')
                    
                    % if reflectionCorrected system type, we need to
                    % correct the reflection in the header here
                    if strcmp(this.getCoordinateSystemType,'reflectionCorrected')
                       
                        reflectionDimsTmp = double(this.getReflectionDims);
                        reflectionDimsTmp(reflectionDimsTmp == 1) = -1;
                        reflectionDimsTmp(reflectionDimsTmp == 0) = 1;
                        
                        newMatrix = Rotation.matrix * diag(reflectionDimsTmp);
                        
                        Rotation.translationVector = Rotation.translationVector .* reflectionDimsTmp;
                        Rotation.matrix = newMatrix;
                        Rotation.affine4x4Matrix = eye(4,4);
                        Rotation.affine4x4Matrix(1:3,1:3) = diag(this.getvoxelsize) * Rotation.matrix;
                        Rotation.affine4x4Matrix(1:3,4) = Rotation.translationVector;
                        OptsVRROT.epsilon = 1e-6;
                        vectorExpressionTmp = real(vrrotmat2vec(Rotation.matrix,OptsVRROT));
                        Rotation.axis  = vectorExpressionTmp(1:3);
                        Rotation.angle = rad2deg(vectorExpressionTmp(4));
                        Rotation.eulerAngles = rad2deg(orth2ang(Rotation.matrix));
                        Rotation.systemType = 'reflectionCorrected';
                    end
                end
            else
                error('importHeaderType property not set.')
            end
        end
        
        
        
        
        function projectionLength = getprojectionLength(obj)
            % returns the projection length of the current img volume. If
            % no projectionLength is specified it is assumed that no
            % projection is performed (i.e., projectionLength =1)
            if isempty(obj.processingFileInfo)
                projectionLength = 1;
            elseif isempty(obj.processingFileInfo.projectionLength)
                projectionLength = 1;
            else
                projectionLength = obj.processingFileInfo.projectionLength;
            end
        end
        
        
        function echoTimes = getEchoTimes(obj,echoNo)
            % returns voxels size of the data set
            if isempty(obj.echoTimes)
                if ~isempty(obj.getHeader('NIFTI'))
                    echoTimes = [];
                elseif ~isempty(obj.getHeader('DICOM'))
                    try
                        [echoTimes, echoTimesOrder] = unique(arrayfun(@(x) x.EchoTime, obj.getHeader('DICOM')));
                        [~,echoTimesOrderIdx] = sort(echoTimesOrder);
                        echoTimes(echoTimesOrderIdx) = echoTimes; % this is needed because unique sorts them!
                        if isfield(obj.getHeader('DICOM'),'EchoNumber')
                            echoNumbers = unique(arrayfun(@(x) x.EchoNumber, obj.getHeader('DICOM')));
                        else
                            warning('DICOM header does not contain field EchoNumber. Setting number of echoes equal to number of different echo times. Consequences not known...')
                            echoNumbers = 1:numel(echoTimes);
                        end
                    catch
                        echoTimes = unique(cellfun(@(x) x.EchoTime, obj.getHeader('DICOM')));
                        try
                            echoNumbers = unique(cellfun(@(x) x.echoNumber, obj.getHeader('DICOM')));
                        catch
                            echoNumbers = unique(cellfun(@(x) x.EchoNumber, obj.getHeader('DICOM')));
                        end
                    end
                    if numel(echoTimes) ~= numel(echoNumbers)
                        echoTimes = echoTimes(echoTimes > 0);
                        if numel(echoTimes) ~= numel(echoNumbers)
                            warning('Matlab:mids', 'The number of detected echo times does not correspond to the number of detected echo numbers.')
                            obj.setEchoNumbers;
                        end
                    end
                elseif  ~isempty(obj.getHeader('BRUKER'))
                    echoTimes = obj.getHeader('BRUKER').acqp.ACQ_echo_time';
                    elseif  ~isempty(obj.getHeader('ICERAW'))
                    echoTimes = obj.getHeader('ICERAW').echoTimes;
                else
                    warning('Matlab:mids', 'There is no echo time specified in this mids object. Setting the echo times to []');
                    echoTimes  = [];
                end
                if ~isempty(echoTimes) && nnz(echoTimes < 0.1) > 0
                    warning(['Echo times seem to be specified in seconds instead of in ms (largest detected echo time: ' num2str(min(echoTimes)) '). This seems to occur sometimes with SIEMENS 7T data. Echo times will be multiplied by 1000.']);
                    echoTimes = echoTimes * 1000;
                end
            else
                echoTimes = obj.echoTimes;
            end
            if nargin > 1 && ~isempty(echoNo) && ~isempty(echoTimes)
                echoTimes = echoTimes(echoNo);
            end
        end
        
        function coordinateSystemType = getCoordinateSystemType(obj)
            coordinateSystemType =  obj.coordinateSystem;
        end
        
        
        function InformationSeq = getSequenceInformation(obj)
            headerDecoded = decodePrivateTags(obj.getHeader('DICOM',1));
            InformationSeq.acquisitionTime = headerDecoded.Private_0051_100a;
            InformationSeq.sliceOrientation = headerDecoded.Private_0051_100e;
            InformationSeq.coilSelections = headerDecoded.Private_0051_100f;
        end
        
        
        
        function headerType = getImportHeaderType(this)
            headerType = this.importHeaderType;
        end
        
        %% set methods
        function obj = setvoxelsize(obj, voxelsize)
            % sets the voxel size of the image data block
            if isempty(obj.voxelsizeOriginal)
                obj.voxelsizeOriginal = voxelsize;
            end
            obj.voxelsize = voxelsize;
        end
                
        function this = setOrientation(this,Rotation)
            % sets the rotation structure
            
            
            OptsVRROT.epsilon = 1e-6;
            vectorExpressionTmp = real(vrrotmat2vec(Rotation.matrix,OptsVRROT));
            
            if sum(abs(vectorExpressionTmp(1:3))) == 0
                warning('Rotation axis cannot be calculated for this rotation matrix.')
            else
                if ~isfield(Rotation,'axis') || ~isfield(Rotation,'angle')
                    Rotation.axis  = vectorExpressionTmp(1:3);
                    Rotation.angle = rad2deg(vectorExpressionTmp(4));
                    dispstatus('Axis and angle calculated from supplied matrix.')
                end
            end
            
            if ~isfield(Rotation,'eulerAngles')
                Rotation.eulerAngles = rad2deg(orth2ang(Rotation.matrix));
                dispstatus('Euler angles calculated from supplied matrix.')
            end
            
            
            this.Rotation = Rotation;
            
        end
        
        function obj = setEchoTimes(obj, echoTimes)
            % sets the echo times of the image data block
            obj.echoTimes = echoTimes;
        end
        
        function obj = setMagneticFieldStrength(obj, magneticFieldStrength)
            % sets the magnetic field strength of the image data block
            obj.magneticFieldStrength = magneticFieldStrength;
        end
        
        
        function this = setObjectOrientation(this,strOrientation)
            % http://alex-ihpc.blogspot.de/2010/12/22-december-2010.html
            switch strtrim(sort(strOrientation))
                case 'FHS' % head first-supine:
                    % feet-to-head is increasing z
                    % right-to-left is increasing x
                    % anterior-posterior is increasing y
                    this.objectOrientation = strOrientation;
                case 'FHP' % head first-prone
                    this.objectOrientation = strOrientation;
                case 'DFHR' % head first-decibitus right
                    this.objectOrientation = strOrientation;
                case 'DFHL' % head first-decibitus left
                    this.objectOrientation = strOrientation;
                case 'FFP' % feet first-prone
                    this.objectOrientation = strOrientation;
                case 'FFS' % feet first-supine
                    this.objectOrientation = strOrientation;
                case 'DFFR' 
                    this.objectOrientation = strOrientation;
                case 'DFFL' 
                    this.objectOrientation = strOrientation;
                case 'hinpsx' % sphinx
                    this.objectOrientation = strOrientation;
                otherwise
                    error(['Object orientation ' strOrientation ' not yet supported.'])
            end
        end
        
        
        
        function obj = setImportRoutine(obj, importRoutine)
            % sets the echo times of the image data block
            obj.importRoutine = importRoutine;
        end
        
        function obj = setCoordinateSystemType(obj, coordinateSystemType)
            % sets the coordinateSystem type
            switch coordinateSystemType
                case {'DICOM','NIFTI','BRUKER', 'ICERAW', 'unknown','reflectionCorrected','TOSHIBA'}
                    obj.coordinateSystem = coordinateSystemType;
                otherwise
                    error('System type currently not supported')
            end
        end
        
        
        
        function setMatrixOrientation(this,orientation)
            switch orientation
                case {'transversal','coronal','sagittal'}
                    if ~isempty(this.matrixOrientation) && ~strcmp(orientation,this.matrixOrientation)
                        warning(['Overwriting matrix orientation! Was: ' this.matrixOrientation '; Now: ' orientation])
                    end
                    
                    if isempty(this.matrixOrientationOriginal)
                        this.matrixOrientationOriginal = orientation;
                    end
                    this.matrixOrientation = orientation;
                otherwise
                    error('Matrix orientation type unknown.')
            end
        end
        
        
        
        function this = setImportHeaderType(this, importHeaderType)
            % sets the header type which belongs to the data
            % This property is absolutely crucial for having correct
            % orientation information. The DICOM orientation information
            % may not be used if the data matrix was imported from NIFTI,
            % and vice versa!
            switch importHeaderType
                case {'DICOM','NIFTI', 'BRUKER', 'ICERAW', 'RAW','none'}
                    this.importHeaderType = importHeaderType;
                otherwise
                    error('Import header type currently not supported')
            end
        end
        
        function this = setReflectionDims(this, reflectionDims)
            if ~isempty(reflectionDims) && islogical(reflectionDims) && numel(reflectionDims) == 3
                this.reflectionDims = reflectionDims;
            else
                error('No valid input')
            end
        end
        
        function retVal = getReflectionDims(this)
            retVal = this.reflectionDims;
        end
        
        function this = switchCoordinateSystemType(this, newSystemType)
            
            if strcmp(newSystemType,'reflectionCorrected') && isempty(this.reflectionDims)
                this.reflectionDims = determineimproperrotationreflection(this.getOrientation.matrix);
            end
            
            
            % switches the coordinateSystem type
            switch this.getCoordinateSystemType
                case 'NIFTI'
                    switch newSystemType
                        case 'DICOM'
                            this.img = flipdim(flipdim(this.img,1),2);
                            this.setCoordinateSystemType('DICOM');
                            % difference between NIFTI and DICOM systems is that x and
                            % y are flipped
                        case 'NIFTI'
%                             this.img = flipdim(flipdim(this.img,2),3);
%                             this.setCoordinateSystemType('NIFTI');
                        case 'BRUKER'
                            this.img = flipdim(flipdim(this.img,1),2);
%                             this.img = permute(this.img,[1 3 2 4 5 6 7 8 9]);
                             this.setCoordinateSystemType('BRUKER');
                        case 'reflectionCorrected' % reflections maintained in the orientation matrix are compensated for
                            if this.reflectionDims(1)
                                this.img = flipdim(this.img,1);
                            end
                            if this.reflectionDims(2)
                                this.img = flipdim(this.img,2);
                            end
                            if this.reflectionDims(3)
                                this.img = flipdim(this.img,3);
                            end
                            this.setCoordinateSystemType('reflectionCorrected');
                            this.coordinateSystemTypeBeforeReflectionCorrection = 'NIFTI';
                        otherwise
                            error('Target system type not supported.')
                    end
                case 'DICOM'
                    switch newSystemType
                        case 'DICOM'
                        case 'NIFTI'
                            this.img = flipdim(flipdim(this.img,1),2);
                            this.setCoordinateSystemType('NIFTI');
                        case 'BRUKER' % bruker is same as DICOM
                            %this.img = flipdim(flipdim(flipdim(this.img,2),3),1);
                            this.setCoordinateSystemType('BRUKER');
                        case 'ICERAW'
                            this.img = flipdim(flipdim(this.img,1),2);
                            this.setCoordinateSystemType('ICERAW');
                        case 'reflectionCorrected' % reflections maintained in the orientation matrix are compensated for
                            if this.reflectionDims(1)
                                this.img = flipdim(this.img,1);
                            end
                            if this.reflectionDims(2)
                                this.img = flipdim(this.img,2);
                            end
                            if this.reflectionDims(3)
                                this.img = flipdim(this.img,3);
                            end
                            this.setCoordinateSystemType('reflectionCorrected');
                            this.coordinateSystemTypeBeforeReflectionCorrection = 'DICOM';
                        otherwise
                            error('Target system type not supported.')
                    end
                case 'BRUKER'
                    switch newSystemType
                        case 'DICOM'
                            %this.img = flipdim(flipdim(flipdim(this.img,2),3),1);
                            this.setCoordinateSystemType('DICOM');
                            % difference between NIFTI and DICOM systems is that x and
                            % y are flipped
                        case 'NIFTI'
                             this.img = flipdim(flipdim(this.img,2),1);
                             this.setCoordinateSystemType('NIFTI');
                        case 'BRUKER'
                        case 'ICERAW'
                        case 'reflectionCorrected' % reflections maintained in the orientation matrix are compensated for
                            if this.reflectionDims(1)
                                this.img = flipdim(this.img,1);
                            end
                            if this.reflectionDims(2)
                                this.img = flipdim(this.img,2);
                            end
                            if this.reflectionDims(3)
                                this.img = flipdim(this.img,3);
                            end
                            this.setCoordinateSystemType('reflectionCorrected');
                            this.coordinateSystemTypeBeforeReflectionCorrection = 'BRUKER';
                        otherwise
                            error('Target system type not supported.')                            
                    end
                case 'ICERAW'
                    switch newSystemType
                        case 'DICOM'
                            this.img = flipdim(flipdim(this.img,1),2);
                            this.setCoordinateSystemType('DICOM');
                            % difference between NIFTI and DICOM systems is that x and
                            % y are flipped
                        case 'NIFTI'
                        case 'BRUKER'
                            this.img = flipdim(this.img,3);
                        case 'ICERAW'
                        case 'reflectionCorrected' % reflections maintained in the orientation matrix are compensated for
                            if this.reflectionDims(1)
                                this.img = flipdim(this.img,1);
                            end
                            if this.reflectionDims(2)
                                this.img = flipdim(this.img,2);
                            end
                            if this.reflectionDims(3)
                                this.img = flipdim(this.img,3);
                            end
                            this.setCoordinateSystemType('reflectionCorrected');
                            this.coordinateSystemTypeBeforeReflectionCorrection = 'ICERAW';
                        otherwise
                            error('Target system type not supported.')                            
                    end
                case 'reflectionCorrected'
                    if strcmp(newSystemType,this.coordinateSystemTypeBeforeReflectionCorrection) || strcmp(newSystemType,'original')
                        if this.reflectionDims(1)
                            this.img = flipdim(this.img,1);
                        end
                        if this.reflectionDims(2)
                            this.img = flipdim(this.img,2);
                        end
                        if this.reflectionDims(3)
                            this.img = flipdim(this.img,3);
                        end
                        this.setCoordinateSystemType(this.coordinateSystemTypeBeforeReflectionCorrection);
                    elseif strcmp(newSystemType,'reflectionCorrected')
                        % do nothing
                    else
                        error(['Coordinate system can only be switched from reflectionCorrected to ' this.coordinateSystemTypeBeforeReflectionCorrection ' (the original one).'])
                    end
                otherwise
                    error('Internal error in switchSystem member function: Unknwon system type.')
            end
        end
        
        
        
        function this = setHeader(this,HeaderVar)
            % sets the header property
            
            if isfield(HeaderVar(1),'AcquisitionTime') || iscell(HeaderVar(1))
                % is DICOM header
                this.hdrDcm = HeaderVar;
            elseif isfield(HeaderVar,'dime')
                % is NIFTI header
                this.hdr = HeaderVar;
            elseif isfield(HeaderVar, 'acqp')
                % is BRUKER header
                this.hdrBruker = HeaderVar;                
            elseif isfield(HeaderVar, 'lBaseResolution')
                % is ICERAW header
                this.hdrIceRaw = HeaderVar;
            elseif isfield(HeaderVar, 'inversionTime')
                % is Toshiba raw data
                this.hdrRaw = HeaderVar;
            elseif isfield(HeaderVar, 'mrs_ref_rgn')
                % is Toshiba raw spectroscopic data
                this.hdrRaw = HeaderVar;
            else
                error('This header type is not supported.')
            end
        end
        
        
        function this = setInformalHeader(this,HeaderVar)
            % sets the header property for informal headers (i.e. headers
            % that not necessarily fit the data)
            
            if isfield(HeaderVar(1),'AcquisitionTime') || iscell(HeaderVar(1))
                % is DICOM header
                this.hdrDcmInformal = HeaderVar;
            elseif isfield(HeaderVar,'dime')
                % is NIFTI header
                this.hdrInformal = HeaderVar;
            elseif isfield(HeaderVar, 'acqp')
                % is BRUKER header
                this.hdrBrukerInformal = HeaderVar;                
            elseif isfield(HeaderVar, 'lBaseResolution')
                % is ICERAW header
                this.hdrIceRawInformal = HeaderVar;
            else
                error('This header type is not supported.')
            end
        end
        
        function this = setEchoNumbers(this)
            % this function sets the echo numbers in the DICOM header in
            % accordance with the echo times.
            if ~isempty(this.getHeader('DICOM'))
                try
                    TEs= unique(arrayfun(@(x) x.EchoTime, this.getHeader('DICOM')));
                    echoNumbers = unique(arrayfun(@(x) x.EchoNumber, this.getHeader('DICOM')));
                catch
                    TEs = unique(cellfun(@(x) x.EchoTime, this.getHeader('DICOM')));
                    echoNumbers = unique(cellfun(@(x) x.echoNumber, this.getHeader('DICOM')));
                end
                if numel(TEs) ~= numel(echoNumbers)
                    TEs = TEs(TEs > 0);
                    if numel(TEs) ~= numel(echoNumbers)
                        warning('Matlab:mids', 'The number of detected echo times does not correspond to the number of detected echo numbers.')
                        header = this.getHeader;
                        echoNumbers = [1:length(TEs)];
                        for i = 1:length(header)
                            TE   = header(i).EchoTime;
                            TEno = echoNumbers(TEs == TE);
                            header(i).EchoNumber = TEno;
                        end
                        this.setHeader(header);
                    end
                end
            end
        end
        
        %% other methods
        
        function this = dump(this,diskDumpFileName,isEraseImg)
            % dumps mids to disk
            
            if nargin > 1
                this.diskDumpFile = diskDumpFileName;
            end
            
            if ~isempty(this.diskDumpFile)
                save(this.diskDumpFile,'this','-v7.3');
                if nargin < 3 || isempty(isEraseImg) || isEraseImg
                    this.img = [];
                end
                this = this.diskDumpFile;
            else
                error('Please set diskDumpFile property first.')
            end
        end
        
        
        function this = dedump(this)
            % gets dump of mids to disk
            buffer = load(this.diskDumpFile);
            this = buffer.this.clone;
        end
        
        
        function [retBool,checkReport] = check(this,type,level)
            dispstatus('mids.check',[],'functioncall')
            retBool = true;
            checkReport.type = type;
            checkReport.level = level;
            switch type
                case 'rawQSMInput'
                    switch level
                        case 'relaxed'
                            dispstatus('No tests performed...')
                        case 'strict'
                            
                            
                            % check for data matrix orientation (z-axis
                            % must be roughly B0, because otherwise,
                            % e.g., bet will fail)
                            statusStr = 'Availability of data matrix orientation information';
                            try
                                this.getMatrixOrientation;
                                dispstatus(statusStr,[],'passed')
                                checkReport = setfield(checkReport,'matrixOrient',1);
                            catch err
                                dispstatus(statusStr,[],'failed')
                                dispstatus(['Reason: ' err.message])
                                retBool = false;
                                checkReport = setfield(checkReport,'matrixOrient',0);
                            end
                            
                            % check if header orientation is proper, i.e. does not
                            % contain reflections (det(matrix) = 1).
                            statusStr = 'Data orientation matrix is proper';
                            reflectionVec = determineimproperrotationreflection(this.getOrientation.matrix);
                            if nnz(reflectionVec)
                                dispstatus(statusStr,[],'failed')
                                retBool = false;
                                checkReport = setfield(checkReport,'matrixProper',0);
                            else
                                dispstatus(statusStr,[],'passed')
                                checkReport = setfield(checkReport,'matrixOrient',1);
                            end
                            
                            
                            header = this.getHeader('DICOM');
                            
                            if ~isempty(header) % --> there is a DICOM header
                                if iscell(header)
                                    header = header{1};
                                else
                                    header = header(1);
                                end
                                
                                
                                % check for bad symbols in the raw DICOM
                                % path (will cause errors when converting
                                % header to NII)
                                statusStr = 'Availability of original DICOM path information';
                                if isfield(header,'Filename')
                                    statusStr = 'Check for spaces in original DICOM path';
                                    if nnz(isspace(header.Filename))
                                        dispstatus(statusStr,[],'failed')
                                        retBool = false;
                                    else
                                        dispstatus(statusStr,[],'passed')
                                    end
                                    checkReport = setfield(checkReport,'originalDicomPathSpaces',1);
                                else
                                    dispstatus(statusStr,[],'failed')
                                    retBool = false;
                                    checkReport = setfield(checkReport,'originalDicomPathSpaces',0);
                                end
                                
                                % check if image interpolation was switched
                                % on during scanning. Interpolation results
                                % in a mismatch of the header information
                                % used throughout the toolbox (e.g. in
                                % determination of k-space interpolation)
                                % with data matrix size. This is not a
                                % principal problem and may be resolved in
                                % future if required.
                                if isfield(header,'Private_0051_100b') && ~isempty(header.Private_0051_100b)
                                    statusStr = 'Check for spatial interpolation';
                                    dimInterp(1) = str2double(header.Private_0051_100b(1:3));
                                    dimInterp(2) = str2double(header.Private_0051_100b(strfind(header.Private_0051_100b,'*')+1:strfind(header.Private_0051_100b,'s')-1)); % e.g. was 364p*448s 
                                    hdrAcqMtx = double(header.AcquisitionMatrix(header.AcquisitionMatrix ~= 0));
                                    if (dimInterp(1) ~= hdrAcqMtx(2)) ...
                                            || (dimInterp(2) ~= hdrAcqMtx(1))
                                        dispstatus(statusStr,[],'failed')
                                        retBool = false;
                                    else
                                        dispstatus(statusStr,[],'passed')
                                    end
                                    checkReport = setfield(checkReport,'spatialInterpolation',1);
                                else
                                    % if no information present, we assume
                                    % there is no interpolation
                                    dispstatus(statusStr,[],'passed')
                                    retBool = true;
                                    checkReport = setfield(checkReport,'spatialInterpolation',0);
                                end
                            end
                            
                        otherwise
                            error(['Checking level ' level ' currently not supported for check-type ' type '.'])
                    end
                otherwise
                    error('Checking type not supported.')
            end
            checkReport.passed = retBool;
            dispstatus('mids.check',[],'functionexit')
        end
        
    end
end
