function constants = getconstants(systemType)

%GETCONSTANTS Returns struct of physical constants.
%
%   GETCONSTANTS Struct of physical constants.
%
%
%   Syntax
%
%   GETCONSTANTS()
%
%
%   Description
%
%   GETCONSTANTS() returns struct with physical constants in SI-system.
%
%


%   2009/04/30 F Schweser, mail@ferdinand-schweser.de
%   v1.1 - 2009/11/23 - FS - Vacuum permeability added.



if nargin < 1 || isempty(systemType)
    systemType = 'SI';
end



if strcmp(systemType,'SI')
    constants.gyromagneticRatio = 2.67522128e8; % this is really gamma, not gamma/2/pi! i.e. f = gamma/2/pi*B, omega = gamma*B
    constants.vacuumPermeability = 1.256637061e-6;
    
    % susceptibility difference off fully oxygenated and deoxygenated
    % blood (Sedlacick's PhD thesis (SI) 2.49ppm)
    constants.deltaSusceptibilityBloodOxyDeOxy = 2.49e-6;
    constants.Susceptibility.water = -9.035e-6;
else
    error('Wrong unit system.')
end
end