function [inputObj coneMask angularLaplacianRealOut unwrappedPhase sharpPhase] = laplacianphaseprocessing(inputObj,Options)

%LAPLACIANPHASEPROCESSING Processes measured phase data with Laplacian calculus.
%
%   LAPLACIANPHASEPROCESSING Processes measured phase data with Laplacian
%   calculus.
%
%
%   Syntax
%
%   B = LAPLACIANPHASEPROCESSING(A)
%   B = LAPLACIANPHASEPROCESSING(A, Options)
%
%
%   Description
%
%   B = LAPLACIANPHASEPROCESSING(A) returns B which is a
%   unwrapping-estimation of the wrapped 3D (mids) phase object A.
%
%   B = LAPLACIANPHASEPROCESSING(A,Options) uses Options defined in the
%   Options-struct. The following fields are currently supported:
%
%       mask        - binary mask which will be applied after first
%                     Laplacian calculation. If SHARP-processing is
%                     desired, set mask equal to an eroded (>0 binary erosion) 
%                     tissue mask (cf. internal  mask).
%       isSymmetryMode - switch that activates symmetry mode (DCT). This
%                     mode yields improved unwrapped phase must demands 8x
%                     as much memory (default: off) [not tested thoroughly]
%       laplacianTruncationValue - Defines regularization value of the
%                     inverse Laplacian operation (default is taken from 
%                     LOOKUPSHARPREGULARIZATIONPARAM)
%       isQSM       - switch that activates Fourier-based QSM (Shmueli et
%                     al. 2009 MRM). The following additional parameters
%                     may be supplied:
%                       Rotation           - Orientation-struct of the data
%                                            slab orientation (default:
%                                            automatic).
%       qsmAlgorithm - string that specifies the algorithmic approach to
%                      perform the inversion. The following approaches are
%                      supported:
%                           'inverse_filtering' (default) - This is the conventional
%                           approach according to Shmueli et al. 2009 MRM.
%                           
%                           'gradient_l2_regularized_2point' - This approach
%                           regularizes the 2-point gradient of the susceptibility
%                           in a L2-norm sense, according to Bilgic JMRI
%                           2013. Note: In this case a regularization
%                           parameter must be chosen. See
%                           qsmAlgorithmParameter for details.
%                           
%                           'gradient_l2_regularized_3point' - This approach
%                           regularizes the 3-point gradient of the susceptibility
%                           in a L2-norm sense. The 3point gradient produces 
%                           slightly crispier images than the 2point one.
%                           Note: In this case a regularization
%                           parameter must be chosen. See
%                           qsmAlgorithmParameter for details.
%       qsmAlgorithmParameter - This field sets a regularization parameter
%                               for the regularized qsmAlgorithm. Default:
%                               0.015/SDIcorrectionfactor.
%       isOnlyQSM   - if true, only QSM is preformed, no phase processing.
%                     In this case input should be SHARP phase. (default:
%                     false)
%                     Setting this field to true will automatically set
%                     isQSM to true.
%       laplacianFt - Fourier domain representation of the Laplacian
%                    operator. This option can be used if the Laplacian
%                    operator is already available (speedup).
%
%       dipoleResponseFt - Fourier domain representation of the unit dipole
%                    response. This option can be used if the dipole
%                    response is already available (speedup).
%
%       DipoleFilter - Dipole inversion regularization options. Please see
%                    GENERATEUNITDIPOLERESPONSE() for details. Default:
%                       .type = 'threshSingularValues'
%                       -parameter = 2/3
%
%       Rotation     - Slice orientation struct
%
%       applyCorrectionFactor - If true calculates correction factor and
%                    applies to QSM. (default: true)
%
%
% Not: this function supports in-place call for improved performance. This
%      means that the input object will be overwritten. Use .clone to avoid
%      this.
%
% This function is an implementation of the Fourier unwrapping algorithm
% introduced by Schofield M and Zhu Y, Optics Letters 2003 28(14):1194-6.
% The algorithm was extended with the SHARP-approach (Schweser F et al,
% Neuroimage 2011) and with the Fourier-based QSM approach by Shmueli (see
% above).


%
% F Schweser, mail@ferdinand-schweser.de
%
%   Changelog:
%
%		2011/09/14 - v1.0 - F Schweser
%       2011/09/20 - v1.1 - F Schweser - runtime optimizations
%       2011/09/23 - v1.2 - F Schweser - Rotation detection automized
%       2011/10/11 - v1.3 - F Schweser - Now also returns the unwrapped
%                                        phase + Works with int16 +
%                                        default parameter for dipole
%                                        filter
%       2011/10/13 - v1.4 - F Schweser - new feature applyCorrectionFactor

%       2011/10/26 - v1.41 - F Schweser - normalization of Laplacian parameter, added Rotation field to doc
%       2011/10/26 - v1.42 - A Deistung - try catch if orientation-information is not
%                                        correct in the mids-object

%       2012/01/19 - v1.5 - F Schweser - Now automatically makes odd
%       2012/02/08 - v1.6 - F Schweser - Bugfix: conversionFactor was
%                                        applied also in case of unwrapping
%       <2012/04/20 - v1.7 - F Schweser - Now tries to extract orientation information
%					 from mids object and pipes isFourierDomainFormula
%       2012/07/04 - v1.8 - F Schweser - Option isOnlyQSM added.
%       2012/07/18 - v1.9 - F Schweser - Final masking removed
%       2012/08/20 - v2.0 - F Schweser - Bugfix symmetryMode
%       2012/10/29 - v2.1 - F Schweser - Normalization of voxelsize during generation of Laplacian-FT removed. This resulted in worse quality in Graz in situ data
%       2012/12/04 - v2.2 - F Schweser - Removed lookup of Laplacian
%                                        regularization parameter and set
%                                        default to 0.001 because the
%                                        unwrapping does not require
%                                        SHARP-regularization
%       2013/03/20 - v2.3 - F Schweser - Added input data check +
%                                        sharpPhase geunodded
%       2013/06/04 - v2.4 - F Schweser - Minor bugfix that doesn't seem to
%                                        have major effect on
%                                        susceptibility (0.7 --> 2/3)
%       2013/10/23 - v3.0 - F Schweser - Now supports regularized inversion.
%       2013/11/12 - v3.1 - F Schweser - Minor modification echoTimes
%       2021/08/17 - v3.2 - F Schweser - Major bugfix regarding corr factor


%% Check Input Parameters

dispstatus(mfilename(),[],'functioncall')

DEFAULT_ISONLYQSM = false;
DEFAULT_QSMALGORITHMTYPE = 'inverse_filtering';
DEFAULT_LAMBDA =  0.015;    % regularization parameter: chosen with L-curve heuristic (taken from Berkin's demoscript)
OptsL.Laplacian = '2';

DEFAULT_LAPLACIAN_TRUNCATION = 0.001;%lookupsharpregularizationparam(1,'2');

if nargin < 2 || isempty(Options)
    Options.dummy = [];
end

if sum(parity(size(inputObj.img)) - 3)
    [inputObj.img unOdd] = makeodd(inputObj.img);
else
    unOdd = [1 1 1];
end

if ~isfield(Options,'mask') || isempty(Options.mask)
    mask = 1;
else
    mask = makeodd(Options.mask);
end

if ~isfield(Options,'qsmAlgorithm') || isempty(Options.qsmAlgorithm)
    Options.qsmAlgorithm = DEFAULT_QSMALGORITHMTYPE;
end


if ~isfield(Options,'isOnlyQSM') || isempty(Options.isOnlyQSM)
    Options.isOnlyQSM = DEFAULT_ISONLYQSM;
elseif Options.isOnlyQSM
    Options.isQSM = true;
else
    if max(inputObj.img(:))-min(inputObj.img(:)) > 2*pi
    error('Data valuie range must be 2pi!!!')
    else
        inputObj.img = adjGV(inputObj.img,[-pi pi]);
    end
end


if ~isfield(Options,'Rotation') || isempty(Options.Rotation)

    try
        Rotation = inputObj.getOrientation;
    catch
        dispstatus('Transversal orientation DEFAULT.')
        Rotation = [];
    end

else
    Rotation = Options.Rotation;
end

if ~isfield(Options,'DipoleFilter') || isempty(Options.DipoleFilter)
        Options.DipoleFilter.type = 'SDI';
        %Options.DipoleFilter.parameter = 2/3;
end

if ~isfield(Options,'isQSM') || isempty(Options.isQSM)
    Options.isQSM = false;
end

if ~isfield(Options,'isFourierDomainFormula') || isempty(Options.isFourierDomainFormula)
    OptD.isFourierDomainFormula = true;
else
    OptD.isFourierDomainFormula = Options.isFourierDomainFormula;
end

if ~isfield(Options,'isSymmetryMode') || isempty(Options.isSymmetryMode)
    Options.isSymmetryMode = false;
end

if ~isfield(Options,'laplacianTruncationValue') || isempty(Options.laplacianTruncationValue)
    Options.laplacianTruncationValue = DEFAULT_LAPLACIAN_TRUNCATION;
end

voxelsize = inputObj.getvoxelsize;
arraySize = size(inputObj.img);
corrFactorCenter = 1;
if Options.isQSM
    if ~isfield(Options,'dipoleResponseFt') || isempty(Options.dipoleResponseFt)
        dispstatus('Generate unit dipole',[],'status')
        
        OptD.Rotation = Rotation;
        OptD.voxelAspectRatio = voxelsize;
        OptD.DipoleFilter = Options.DipoleFilter;
        Options.dipoleResponseFt = generateunitdipoleresponse(arraySize,true,OptD);
    end
    
    
    if ~isfield(Options,'applyCorrectionFactor') || isempty(Options.applyCorrectionFactor)
        Options.applyCorrectionFactor = true;
    end
    
    if Options.applyCorrectionFactor && (strcmp(Options.DipoleFilter.type,'SDI') || Options.DipoleFilter.parameter > 0)
        
        dispstatus('Generate non-regularized unit dipole for correction factor',[],'status')
        
        OptD.Rotation = Rotation;
        OptD.voxelAspectRatio = voxelsize;
        OptD = rmfield(OptD,'DipoleFilter');
        Options.dipoleResponseFtNR = generateunitdipoleresponse(arraySize,true,OptD);
        
        %dipoleMask = abs(Options.dipoleResponseFt) == abs(Options.dipoleResponseFtNR);%Options.DipoleFilter.parameter;
        dipoleFactor = Options.dipoleResponseFtNR ./ Options.dipoleResponseFt;
        %dipoleFactor = dipoleMask + ~dipoleMask .* (abs(Options.dipoleResponseFtNR) ./ Options.DipoleFilter.parameter);
        psf = ifftsave(dipoleFactor);
        centerVoxel = floor(size(Options.dipoleResponseFt)/2)+1;
        corrFactorCenter = psf(centerVoxel(1),centerVoxel(2),centerVoxel(3));
    end
else
    Options.dipoleResponseFt = 1;%
end


if Options.isQSM
    OptsF.echoTimes = inputObj.getEchoTimes;
    OptsF.magneticFieldStrength = inputObj.getMagneticFieldStrength;
    conversionFactor = phase2susceptibilityfactor([],OptsF) * 1e6;
end



if numel(arraySize) > 3 && ~parity(arraySize(4))
    disp('Even number of echoes may not work correctly!')
end
% [inputOdd1 unOdd] = makeodd(inputObj.img);
% inputOdd2 = zeros([size(inputOdd1,1) size(inputOdd1,2) size(inputOdd1,3) (2*size(inputOdd1,4)+1)]);
% %inputOdd = repmat(inputOdd,[1 1 1 2]);
% inputOdd2(:,:,:,1:size(inputOdd1,4)) = inputOdd1(:,:,:,1:size(inputOdd1,4));
% inputOdd2(:,:,:,size(inputOdd1,4)+1) = inputOdd1(:,:,:,size(inputOdd1,4));
% inputOdd2(:,:,:,size(inputOdd1,4)+2:end) = flipdim(inputOdd1(:,:,:,1:size(inputOdd1,4)),4);
% 
% OptsL.Laplacian = '4d';


if ~Options.isOnlyQSM

    if Options.isSymmetryMode
        dispstatus('Symmetry mode pre-processing',[],'status')

        inputOdd3 = zeros([(2*arraySize(1)+1) (2*arraySize(2)+1) (2*arraySize(3)+1)]);
        inputOdd3(1:arraySize(1),1:arraySize(2),1:arraySize(3)) = inputObj.img;
        inputOdd3(arraySize(1)+2:end,1:arraySize(2),1:arraySize(3)) = flipdim(inputObj.img,1);
        inputOdd3(arraySize(1)+2:end,arraySize(2)+2:end,1:arraySize(3)) = flipdim(flipdim(inputObj.img,1),2);
        inputOdd3(arraySize(1)+2:end,arraySize(2)+2:end,arraySize(3)+2:end) = flipdim(flipdim(flipdim(inputObj.img,1),2),3);
        inputOdd3(1:arraySize(1),arraySize(2)+2:end,1:arraySize(3)) = flipdim(inputObj.img,2);
        inputOdd3(1:arraySize(1),arraySize(2)+2:end,arraySize(3)+2:end) = flipdim(flipdim(inputObj.img,2),3);
        inputOdd3(1:arraySize(1),1:arraySize(2),arraySize(3)+2:end) = flipdim(inputObj.img,3);
        inputOdd3(arraySize(1)+2:end,1:arraySize(2),arraySize(3)+2:end) = flipdim(flipdim(inputObj.img,3),1);
        inputObj.img = inputOdd3;

        if mask ~= 1
            mask2 = zeros([(2*size(mask,1)+1) (2*size(mask,2)+1) (2*size(mask,3)+1)]);
            mask2(1:size(mask,1),1:size(mask,2),1:size(mask,3)) = mask;
            mask2(size(mask,1)+2:end,1:size(mask,2),1:size(mask,3)) = flipdim(mask,1);
            mask2(size(mask,1)+2:end,size(mask,2)+2:end,1:size(mask,3)) = flipdim(flipdim(mask,1),2);
            mask2(size(mask,1)+2:end,size(mask,2)+2:end,size(mask,3)+2:end) = flipdim(flipdim(flipdim(mask,1),2),3);
            mask2(1:size(mask,1),size(mask,2)+2:end,1:size(mask,3)) = flipdim(mask,2);
            mask2(1:size(mask,1),size(mask,2)+2:end,size(mask,3)+2:end) = flipdim(flipdim(mask,2),3);
            mask2(1:size(mask,1),1:size(mask,2),size(mask,3)+2:end) = flipdim(mask,3);
            mask2(size(mask,1)+2:end,1:size(mask,2),size(mask,3)+2:end) = flipdim(flipdim(mask,3),1);
            mask = mask2;
        end
        arraySizeOld = arraySize;
        arraySize = size(inputObj.img);
    % else
    %     if nnz(parity(arraySize)) < 3
    %         error(['Please use MAKEODD for preprocessing input data. Current array size is ' num2str(arraySize)])
    %     end
    end




    if ~isfield(Options,'laplacianFt') || isempty(Options.laplacianFt)
        dispstatus('Generate conventional Laplacian operator',[],'status')
        %Options.laplacianFt = fftsave(generatelaplacian3d(arraySize,OptsL,voxelsize(1)/min(voxelsize),voxelsize(2)/min(voxelsize),voxelsize(3)/min(voxelsize)));
        Options.laplacianFt = fftsave(generatelaplacian3d(arraySize,OptsL,voxelsize(1),voxelsize(2),voxelsize(3)));
    %     b = zeros([(2*arraySize(1)+1) (2*arraySize(2)+1) (2*arraySize(3)+1)]);
    %     b((round(arraySize(1)/2)-floor(arraySize(1)/2):round(arraySize(1)/2)+floor(arraySize(1)/2)), (round(arraySize(2)/2)-floor(arraySize(2)/2):round(arraySize(2)/2)+floor(arraySize(2)/2)), (round(arraySize(3)/2)-floor(arraySize(3)/2):round(arraySize(3)/2)+floor(arraySize(3)/2))) = Options.laplacianFt;
    %     Options.laplacianFt = b;
        %Options.laplacianFt = fftsave(generatelaplacian3d(arraySize,OptsL,voxelsize(1)/min(voxelsize),voxelsize(2)/min(voxelsize),voxelsize(3)/min(voxelsize)));
    end


    dispstatus('Calculate and apply trigonometric Laplacian operator',[],'status')
    inputObj.img = adjGV(double(inputObj.img),[-pi pi]);
    a = sin(inputObj.img);
    b = cos(inputObj.img);
    angularLaplacianReal = b .* ifftsave(Options.laplacianFt .* fftsave(a)) - a .* ifftsave(Options.laplacianFt .* fftsave(b));
    if nargout > 2
        angularLaplacianRealOut = angularLaplacianReal;
    end

    if nargout > 3
        unwrappedPhase = fftsave(angularLaplacianReal) ./ Options.laplacianFt;
        unwrappedPhase(abs(Options.laplacianFt) <= Options.laplacianTruncationValue) = 0;%< 
        unwrappedPhase = real(ifftsave(unwrappedPhase));
    end


    dispstatus('Masking',[],'status')
    angularLaplacianReal = angularLaplacianReal .* mask;
else
    angularLaplacianRealOut = [];

end

dispstatus('Deconvolution',[],'status')
if ~Options.isOnlyQSM
    dataCpx = fftsave(angularLaplacianReal);
    dataCpx = dataCpx ./ Options.laplacianFt;

    if nargout > 4
        sharpPhase = dataCpx;
        sharpPhase(abs(Options.laplacianFt) <= Options.laplacianTruncationValue) = 0;%< 
        sharpPhase = real(ifftsave(sharpPhase));
        sharpPhase = makeodd(sharpPhase,unOdd);
    end    
else
   dataCpx = fftsave(inputObj.img);
end

if Options.isQSM
    switch Options.qsmAlgorithm
        case 'inverse_filtering'
            dataCpx = dataCpx ./ Options.dipoleResponseFt;
            coneMask = abs(Options.dipoleResponseFt) == 0;
            dataCpx(coneMask) = 0;
        case 'gradient_l2_regularized_2point'
            if ~isfield(Options,'qsmAlgorithmParameter') || isempty(Options.qsmAlgorithmParameter) 
                lambda = DEFAULT_LAMBDA;  
                else
                lambda = Options.qsmAlgorithmParameter;
            end
            lambda = lambda/corrFactorCenter;
            
            dataCpx = regularized_l2_inversefiltering(dataCpx, Options.dipoleResponseFt, lambda, 'gradient_2point');
%             OptsRI.order = 5;
%             OptsRI.kernelSize = [5 5 5];
%             dataCpx = regularized_l2_inversefiltering(dataCpx, Options.dipoleResponseFt, lambda, 'gradient_polynomialfitting',OptsRI);
             coneMask = [];
             
             
        case 'gradient_l2_regularized_3point'
            if ~isfield(Options,'qsmAlgorithmParameter') || isempty(Options.qsmAlgorithmParameter)
                lambda = DEFAULT_LAMBDA;
            else
                lambda = Options.qsmAlgorithmParameter;
            end
            lambda = lambda/corrFactorCenter;
            
            dataCpx = regularized_l2_inversefiltering(dataCpx, Options.dipoleResponseFt, lambda, 'gradient_3point');
            coneMask = [];
        otherwise
            error('This algorithm type is not supported.')
    end
else
    coneMask = [];
end

if ~Options.isOnlyQSM
    dataCpx(abs(Options.laplacianFt) <= Options.laplacianTruncationValue) = 0;%< 
end

inputObj.img = real(ifftsave(dataCpx));
inputObj.img = inputObj.img / corrFactorCenter;
if Options.isSymmetryMode
    
    dispstatus('Post-processing symmetry mode',[],'status')
    inputObj.img = inputObj.img(1:arraySizeOld(1),1:arraySizeOld(2),1:arraySizeOld(3));
    
end
if Options.isQSM
    inputObj.img = inputObj.img .* conversionFactor;
end
inputObj.img = makeodd(inputObj.img,unOdd);

dispstatus(mfilename(),[],'functionexit')

end
