function inversion_lsqr(strAbsFile,strPhsFile,strMskFile,strOutFile,strOutFile_nii,qcFilestem,qcSuffix)
%INVERSION_HEIDI Summary of this function goes here
%   Detailed explanation goes here

mids;
absInput = load(strAbsFile);
phsInput = load(strPhsFile);
mskInput = load(strMskFile);

magnitude = abs(absInput.images.img);
clear absInput


Options.echoTime = phsInput.images.getEchoTimes;
Options.Rotation = phsInput.images.getOrientation;
Options.magneticFieldStrength = phsInput.images.getMagneticFieldStrength;
Options.voxelAspectRatio = phsInput.images.getvoxelsize;


Options.tolerance = 1e-5;
Options.maxit = 400;
Options.offsetUseBool = true;
Options.isFourierDomainFormula = false;
Options.TikhonovRegularizationSusceptibility = [];
Options.solvingType = [];
Options.DipoleFilter = [];
Options.residualWeighting = 0.2 / 9.4 * Options.magneticFieldStrength; % 0.2 optimal in QUASAR paper

DEFAULT_ANTIALIASINGFRAMEWIDTH = 40;


global GLOBAL_PATH_NIFTITOOL_SHEN
GLOBAL_PATH_NIFTITOOL_SHEN = fileparts(mfilename('fullpath'));

global GLOBAL_PATH_FREESURFER
GLOBAL_PATH_FREESURFER = '/opt/freesurfer-6.0.0/';

global GLOBAL_PATH_FSL
GLOBAL_PATH_FSL                     = '/opt/fsl-5.0.8';

suscCompMask = mskInput.mask_tissue.img .* mskInput.mask_phase.img .* phsInput.mask.img;

[suscCompMask,zcInfo] = zealouscrop(suscCompMask);
[suscCompMask,pcInfo] = prepareforconvolution(suscCompMask,[],DEFAULT_ANTIALIASINGFRAMEWIDTH);

magnitude = zealouscrop(magnitude,zcInfo);
magnitude = prepareforconvolution(magnitude,[],DEFAULT_ANTIALIASINGFRAMEWIDTH);

phsInput.images.img = zealouscrop(phsInput.images.img,zcInfo);
phsInput.images.img = prepareforconvolution(phsInput.images.img,[],DEFAULT_ANTIALIASINGFRAMEWIDTH);

susceptibility = phsInput.images.clone;
residual = phsInput.images.clone;
phaseDiscrepancy = phsInput.images.clone;

[susceptibility.img,~, residual.img,~,~,~,~,~,~,phaseDiscrepancy.img] = computesusceptibility_lsqr(phsInput.images.img,Options,logical(suscCompMask),[],magnitude);

%%% convert units to ppm
phase2susceptibilityFactor=phase2susceptibilityfactor([], Options);
susceptibilityRaw = susceptibility.clone;

susceptibility.img = susceptibility.img .* phase2susceptibilityFactor * 1e6; 
susceptibility.img = -susceptibility.img;

susceptibility.img = prepareforconvolution(susceptibility.img,[],[],pcInfo);
susceptibility.img = zealouscrop(susceptibility.img,zcInfo,[],true);
residual.img = prepareforconvolution(residual.img,[],[],pcInfo);
residual.img = zealouscrop(residual.img,zcInfo,[],true);
phaseDiscrepancy.img = prepareforconvolution(phaseDiscrepancy.img,[],[],pcInfo);
phaseDiscrepancy.img = zealouscrop(phaseDiscrepancy.img,zcInfo,[],true);

qc.filestem = qcFilestem;
qc.filesuffix = qcSuffix;

figH = adshow(susceptibility.img(:,:,round(64/7):round(64/7):end-round(64/7)),[-0.1 0.2],[2 3]);
qcFile = fullfile([qc.filestem,'_inv_lsqr_susc_',qc.filesuffix,'.png']);
saveas(figH,qcFile)

figH = adshow(residual.img(:,:,round(64/7):round(64/7):end-round(64/7)),[-0.1 0.1],[2 3]);
qcFile = fullfile([qc.filestem,'_inv_lsqr_resid_',qc.filesuffix,'.png']);
saveas(figH,qcFile)

figH = adshow(phaseDiscrepancy.img(:,:,round(64/7):round(64/7):end-round(64/7)),[-0.1 0.1],[2 3]);
qcFile = fullfile([qc.filestem,'_inv_lsqr_discr_',qc.filesuffix,'.png']);
saveas(figH,qcFile)


save(fullfile(strOutFile),'susceptibility','susceptibilityRaw','residual','phaseDiscrepancy','-v7.3')
write_nii(susceptibility,fullfile(strOutFile_nii))

end

